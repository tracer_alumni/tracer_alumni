$(document).ready(function(){
    $("#notification").click(function(){
        $("#loading-user").addClass("loading-prog");
        $.ajax({
              url : "/Tracer_Alumni/Administrator/header/index",
//            url : "/tracer_alumni/Administrator/header/index",
            cache : false,
            success : function(data){
                $(".dropdown-alerts").html(data);
            },
        });
    });
    
    $("#toggle-sharing").click(function(){
        $("#loading-share").addClass("loading-prog");
        $(".filter-form").toggle();
        $.ajax({
                        url : "/Tracer_Alumni/Administrator/soal/filterAnswer",
//            url : "/tracer_alumni/Administrator/soal/filterAnswer",
            cache : true,
            success : function(data){
                $(".share").html(data);
                $("#loading-share").removeClass("loading-prog");
            },
        });
    });
});