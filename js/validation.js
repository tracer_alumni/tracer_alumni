$(document).ready(function(){
    //var radio = $("#radio").val();
    var check = 0;
    check = $("#checkbox").val();
    if($("#checkbox").val() == undefined)
    {
        check = 0;
    }
    var radio = $("#radio").val();
    $( "form input:radio" ).on( "click", function() {
        if($("form input:radio").is(":checked"))
        {
            radio--;
            if(radio <= 0)
            {
                radio = 0;
                if(check <=0)
                {
                    $("#message").val(" ");
                    $("#submit").removeAttr('disabled');
                }
            }
        }
        $("#radio").val(radio);
    });
    
    $( "form input:checkbox" ).on( "click", function() {
        if($("form input:checkbox").is(":checked"))
        {
            check--;
            if(check <= 0)
            {
                check = 0;
                if(radio <= 0)
                {
                    $("#message").val(" ");
                    $("#submit").removeAttr('disabled');
                }
            }
            else
            {
                $("#message").val("Ada data yang kosong !");
                $("#submit").attr("disabled","disabled");
            }
        }
        else
        {
            check++;
            if(check >= 1)
            {
                $("#submit").attr("disabled","disabled");
                $("#message").text("Ada data yang kosong !");
            }
        }
        $("#checkbox").val(check);
    });
    
    if(check >= 1 || radio >= 1)
    {
        $('#submit').attr('disabled','disabled');
    }
});