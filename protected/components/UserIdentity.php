<?php

class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_role;
    
    public function authenticate()
    {
        $user = Admin::model()->findByAttributes(array('nama_akun' => $this->username));
        if($user === NULL)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else
        {
            if($user->katasandi !== md5($this->password))
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {
                $this->_id = $user->id;
                $this->_role = $user->role;
                $this->errorCode = self::ERROR_NONE;
            }
            $this->setState('__name', $user->nama_akun);
            $this->setState('__role', $this->_role);
        }
        return !$this->errorCode;
    }
    
    public function getId()
    {
        return $this->_id;
    }
    
    public function getRole()
    {
        return $this->_role;
    }
}