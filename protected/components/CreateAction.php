<?php

class CreateAction extends UAction
{
    public $redirectTo = 'index';
    public $modelClass;
    public $formClass;
    public $submit = 'Click';
    public $form = 'form-id';
    
    public function run()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/validation.js',  CClientScript::POS_END);
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $model = new $this->modelClass;
        $form = new CForm($this->requestFormView().$controller->getId().$this->formClass,$model);
        $controller->performAJAXValidation($model,$this->form);
        if($form->submitted($this->submit))
        {
            $model->attributes = $_POST[$this->modelClass];
            $model->created = $this->getDateTime();
            $model->modified = $this->getDateTime();
            
            if($model->save())
            {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Data berhasil diubah');
                $controller->redirect($this->requestBaseUrl().ucfirst($controller->getId()).$this->redirectTo);
            }
            else
            {
                Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_ERROR,'Data gagal untuk diubah');
                $controller->refresh();
            }
        }
        $controller->render($this->_form,array('form' => $form,'model'=> $model));
    }
}