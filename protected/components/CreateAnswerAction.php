<?php

class CreateAnswerAction extends UAction
{
    public $mid;
    public $controller;
    public $modelClass;
    public $criteria;
    public $view;
    public $redirectTo;
    public $prog = '1';
    public $cond1;
    public $resp1;
    public $cond2;
    public $resp2;
    public $session_name;
    public $data = array();
    
    public function run() {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/validation.js',  CClientScript::POS_END);
        Yii::app()->session['progress'] = $this->prog;
        $this->controller = $this->getController();
        $this->checkSessionMhs();
        $model = Soal::model()->findAll($this->criteria);
        $this->controller->performAjaxValidation($model,'form-id');
        if(isset($_POST['Jawaban']))
        {
            if($this->cond1 == NULL || $this->cond2 == NULL)
            {
                $this->resp1 = $this->redirectTo;
                $this->resp2 = $this->redirectTo;
            }
            if($_POST['Jawaban']['validasi_check'] <= 0 && $_POST['Jawaban']['validasi_radio']  <= 0)
            {
                $total = $_POST['Jawaban']['total'];
                for($i=1;$i<=$total;$i++)
                {
                    $jawaban = $_POST['Jawaban'.$i]['pilihan'];
                    $keterangan ='';
                    if(isset($_POST['Jawaban'.$i]['keterangan']))
                    {
                        $val = false;
                        $pil = NULL;
                        $pil = Pilihan::model()->findByPk($jawaban)->pilihan;
                        $val = stripos($pil,"...");
                        if($val)
                        {
                            $keterangan = ($_POST['Jawaban'.$i]['keterangan']);
                        }
                        if($jawaban == $this->cond1)
                        {
                            $this->redirectTo = $this->resp1;
                        }
                        else if($jawaban == $this->cond2)
                        {
                            $this->redirectTo = $this->resp2;
                        }
                     }
                     if(is_array($jawaban))
                    {
                        $count = count($jawaban);
                        $keterangan = $_POST['Jawaban'.$i]['keterangan'];
                        $this->saveModelArray($count, $jawaban, $keterangan,$this->mid);
                    }
                    else
                    {
                        $this->saveModel($jawaban, $keterangan,$this->mid);
                    }
                }
                Yii::app()->session[$this->session_name] = $this->data;
                $this->controller->redirect(array($this->redirectTo));
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Ada data yang kosong ! Mohon diperiksa kembali');
                //$this->controller->refresh();
            }
        }
        $this->controller->render($this->view,array('model' => $model));
    }
    
    public function saveModel($jawaban,$keterangan,$mid)
    {
        $jawaban_model = new Jawaban;
        $jawaban_model->mahasiswa = $mid;
        $jawaban_model->pilihan = $jawaban;
        $jawaban_model->created = $this->getDateTime();
        
        $val = false;
        $pil = Pilihan::model()->findByPk($jawaban)->pilihan;
        $val = stripos($pil,"...");
        if($val)
        {
            $jawaban_model->keterangan = $keterangan;
        }
        if($jawaban == $this->cond1)
        {
            $this->redirectTo = $this->resp1;
        }
        else if($jawaban == $this->cond2)
        {
            $this->redirectTo = $this->resp2;
        }
        $this->data[] = array(
            'pilihan' => $jawaban,
            'created' => $this->getDateTime(),
            'keterangan' => $keterangan,
        );
    }
    
    public function saveModelArray($count,$jawaban,$keterangan,$mid)
    {
        for($in=0;$in<$count;$in++)
        {
            $jawaban_model = new Jawaban;
            $jawaban_model->mahasiswa = $mid;
            $jawaban_model->pilihan = $jawaban[$in];
            $jawaban_model->created = $this->getDateTime();
            $val = false;
            $pil = NULL;
            $pil = Pilihan::model()->findByPk($jawaban[$in])->pilihan;
            $val = stripos($pil,"...");
            if($val)
            {
                $jawaban_model->keterangan = $keterangan;
            }
            if($jawaban == $this->cond1)
            {
                $this->redirectTo = $this->resp1;
            }
            else if($jawaban == $this->cond2)
            {
                $this->redirectTo = $this->resp2;
            }
            $this->data[] = array(
            'pilihan' => $jawaban[$in],
            'created' => $this->getDateTime(),
            'keterangan' => $keterangan
             );
         }
    }
    
    public function checkSessionMhs()
    {
        if(!isset(Yii::app()->session['data_mahasiswa']))
        {
            $this->controller->redirect(array('login'));
            //$this->mid = Yii::app()->session['mahasiswa_id'];
        }
    }
}