<?php

class EWebUser extends CWebUser
{
    protected $_model;
    
    public function role()
    {
        return Yii::app()->user->role;
    }
    
    protected function loadUser()
    {
        if($this->_model == null)
        {
            $this->model = Admin::model()->findByPk($this->id);
        }
        return $this->_model;
    }
    
    public  function getRole()
    {
        return $this->getState('__role');
    }
}