<?php

/**
 * This class is extends CAction 
 * The different is UAction has some variable like $_form to perform default form to form builder,
 * and a function to get a baseUrl , formview
 */

class UAction extends CAction
{
    /**
     * @var string default form to form builder
     */
    public $_form ='/layouts/form';
    
    /**
     * @property default view to render a data provider
     * @var string default list view to render a data provider
     */
    public $_view = '/layouts/listview';
    
    /**
     * 
     * @var string default detail view
     */
    public $_detail = '/layouts/view';
    
    public function requestBaseUrl()
    {
        return Yii::app()->request->baseUrl.'/';
    }
    
    public function requestFormView()
    {
        return 'application.views.';
    }
    
    public function getDateTime()
    {
        return date('Y-m-d h:i:s').'.000000';
    }
}