<?php
/*.
 * bootstrap only extend to configure the run
 * @author Pascal Brewing
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap/widgets
 */

/**
 * Bootstrap alert widget.
 * @see http://twitter.github.com/bootstrap/javascript.html#alerts
 */
class BsAlert extends CWidget
{
    /**
     * @var array the alerts configurations (style=>config).
     */
    public $alerts;
    /**
     * @var string|boolean the close link text. If this is set false, no close link will be displayed.
     */
    public $closeText = BsHtml::CLOSE_TEXT;
    /**
     * @var boolean indicates whether the alert should be an alert block. Defaults to 'true'.
     */
    public $block = true;
    /**
     * @var boolean indicates whether alerts should use transitions. Defaults to 'true'.
     */
    public $fade = true;
    /**
     * @var string[] the JavaScript event configuration (name=>handler).
     */
    public $events = array();
    /**
     * @var array the HTML attributes for the alert container.
     */
    public $htmlOptions = array();

    /**
     * Initializes the widget.
     */
    public function init()
    {
        $this->attachBehavior('BsWidget', new BsWidget);
        $this->copyId();
        if (is_string($this->alerts)) {
            $colors = explode(' ', $this->alerts);
        } else {
            if (!isset($this->alerts)) {
                $colors = array(
                    BsHtml::ALERT_COLOR_SUCCESS,
                    BsHtml::ALERT_COLOR_WARNING,
                    BsHtml::ALERT_COLOR_INFO,
                    BsHtml::ALERT_COLOR_ERROR
                ); // render all styles by default
            }
        }
        if (isset($colors)) {
            $this->alerts = array();
            foreach ($colors as $color) {
                $this->alerts[$color] = array();
            }
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        /* @var $user CWebUser */
        $user = Yii::app()->getUser();
        if (count($user->getFlashes(false)) == 0) {
            return;
        }
        echo BsHtml::openTag('div', $this->htmlOptions);
        foreach ($this->alerts as $color => $alert) {
            if (isset($alert['visible']) && !$alert['visible']) {
                continue;
            }

            if ($user->hasFlash($color)) {
                $htmlOptions = BsArray::popValue('htmlOptions', $alert, array());
                BsArray::defaultValue('closeText', $this->closeText, $htmlOptions);
                BsArray::defaultValue('block', $this->block, $htmlOptions);
                BsArray::defaultValue('fade', $this->fade, $htmlOptions);
                echo BsHtml::alert($color, $user->getFlash($color), $htmlOptions);
            }
        }
        echo '</div>';
        $this->registerEvents("#{$this->htmlOptions['id']} > .alert", $this->events);
    }
}