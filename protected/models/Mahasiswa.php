<?php

/**
 * This is the model class for table "mahasiswa".
 *
 * The followings are the available columns in table 'mahasiswa':
 * @property integer $id
 * @property string $nim
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $prodi
 * @property string $jenis
 * @property string $alamat
 * @property string $kota
 * @property string $telepon
 * @property string $email
 * @property integer $tahun_masuk
 * @property integer $tahun_lulus
 * @property integer $lama_studi
 * @property string $instansi
 * @property string $telepon_instansi
 * @property string $email_instansi
 * @property integer $admin
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Jawaban[] $jawabans
 * @property JawabanKompetensi[] $jawabanKompetensis
 * @property Admin $admin0
 * @property Prodi $prodi0
 * @property Saran[] $sarans
 */
class Mahasiswa extends CActiveRecord
{
    public $from_date;
    public $to_date;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mahasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, nama, tempat_lahir, tanggal_lahir, prodi, jenis, alamat, kota, telepon, email, tahun_masuk, tahun_lulus, instansi, telepon_instansi, email_instansi, created, modified', 'required'),
			array('prodi, tahun_masuk, tahun_lulus, lama_studi, admin', 'numerical', 'integerOnly'=>true),
			array('nim', 'length', 'max'=>12),
			array('nama, tempat_lahir, kota', 'length', 'max'=>45),
			array('jenis', 'length', 'max'=>1),
			array('alamat, email, instansi, email_instansi', 'length', 'max'=>100),
			array('telepon, telepon_instansi', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nim, nama, tempat_lahir, tanggal_lahir, prodi, jenis, alamat, kota, telepon, email, tahun_masuk, tahun_lulus, lama_studi, instansi, telepon_instansi, email_instansi, admin, created, modified', 'safe', 'on'=>'search'),
		);
	}

                public function getYear($begin= NULL,$now = NULL)
        {
            $data = array();
            if($begin == NULL)
            {
                $begin = '1960';
            }
            if($now == NULL)
            {
                $now = date('Y');
            }
            for($i=$begin;$i<=$now;$i++)
            {
                $data[] = array('id' => $i,'tahun' => $i);
            }
            return $data;
        }
        
        public function changeNIMToId($nim)
        {
            $model = Mahasiswa::model()->findByAttributes(array('nim' => $nim));
            if($model == NULL)
            {
                return '1';
            }
            else
            {
                return $model->id;
            }
        }
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jawabans' => array(self::HAS_MANY, 'Jawaban', 'mahasiswa'),
			'jawabanKompetensis' => array(self::HAS_MANY, 'JawabanKompetensi', 'mahasiswa'),
			'admin0' => array(self::BELONGS_TO, 'Admin', 'admin'),
			'prodi0' => array(self::BELONGS_TO, 'Prodi', 'prodi'),
			'sarans' => array(self::HAS_MANY, 'Saran', 'mahasiswa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nim' => 'Nim',
			'nama' => 'Nama',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'prodi' => 'Prodi',
			'jenis' => 'Jenis',
			'alamat' => 'Alamat',
			'kota' => 'Kota',
			'telepon' => 'Telepon',
			'email' => 'Email',
			'tahun_masuk' => 'Tahun Masuk',
			'tahun_lulus' => 'Tahun Lulus',
			'lama_studi' => 'Lama Studi',
			'instansi' => 'Instansi',
			'telepon_instansi' => 'Telepon Instansi',
			'email_instansi' => 'Email Instansi',
			'admin' => 'Admin',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('prodi',$this->prodi);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('tahun_masuk',$this->tahun_masuk);
		$criteria->compare('tahun_lulus',$this->tahun_lulus);
		$criteria->compare('lama_studi',$this->lama_studi);
		$criteria->compare('instansi',$this->instansi,true);
		$criteria->compare('telepon_instansi',$this->telepon_instansi,true);
		$criteria->compare('email_instansi',$this->email_instansi,true);
		$criteria->compare('admin',$this->admin);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
      if(!empty($this->from_date) && empty($this->to_date))
                                           {
                                               $criteria->condition = "created >= '$this->from_date'";
                                           }
                                           else if(!empty ($this->to_date) && empty ($this->from_date))
                                           {
                                               $criteria->condition = "created <= '$this->to_date'";
                                           }
                                           else if(!empty ($this->from_date) && !empty ($this->to_date))
                                           {
                                               $criteria->condition = "created >= '$this->from_date' and created <= '$this->to_date'";
                                           }
                
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mahasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
