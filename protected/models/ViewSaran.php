<?php

/**
 * This is the model class for table "view_saran".
 *
 * The followings are the available columns in table 'view_saran':
 * @property integer $id
 * @property string $saran
 * @property string $nim
 * @property string $nama
 * @property integer $admin
 * @property integer $prodi
 * @property string $nama_prodi
 * @property integer $jurusan
 * @property string $nama_jurusan
 * @property integer $fakultas
 * @property string $nama_fakultas
 * @property string $created
 */
class ViewSaran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'view_saran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('saran, nim, nama, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, created', 'required'),
			array('id, admin, prodi, jurusan, fakultas', 'numerical', 'integerOnly'=>true),
			array('saran', 'length', 'max'=>200),
			array('nim', 'length', 'max'=>12),
			array('nama, nama_prodi', 'length', 'max'=>45),
			array('nama_jurusan, nama_fakultas', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, saran, nim, nama, admin, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'saran' => 'Saran',
			'nim' => 'Nim',
			'nama' => 'Nama',
			'admin' => 'Admin',
			'prodi' => 'Prodi',
			'nama_prodi' => 'Nama Prodi',
			'jurusan' => 'Jurusan',
			'nama_jurusan' => 'Nama Jurusan',
			'fakultas' => 'Fakultas',
			'nama_fakultas' => 'Nama Fakultas',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('saran',$this->saran,true);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('admin',$this->admin);
		$criteria->compare('prodi',$this->prodi);
		$criteria->compare('nama_prodi',$this->nama_prodi,true);
		$criteria->compare('jurusan',$this->jurusan);
		$criteria->compare('nama_jurusan',$this->nama_jurusan,true);
		$criteria->compare('fakultas',$this->fakultas);
		$criteria->compare('nama_fakultas',$this->nama_fakultas,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewSaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
