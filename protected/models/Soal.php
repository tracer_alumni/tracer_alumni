<?php

/**
 * This is the model class for table "soal".
 *
 * The followings are the available columns in table 'soal':
 * @property integer $id
 * @property string $nomor
 * @property string $tipe
 * @property string $kolom
 * @property string $pertanyaan
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property JawabanKompetensi[] $jawabanKompetensis
 * @property Pilihan[] $pilihans
 */
class Soal extends CActiveRecord
{
    public $from_date;
    public $to_date;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'soal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomor, tipe, pertanyaan, created, modified', 'required'),
			array('nomor', 'length', 'max'=>5),
			array('tipe, kolom', 'length', 'max'=>1),
			array('pertanyaan', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomor, tipe, kolom, pertanyaan, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jawabanKompetensis' => array(self::HAS_MANY, 'JawabanKompetensi', 'soal'),
			'pilihans' => array(self::HAS_MANY, 'Pilihan', 'soal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor',
			'tipe' => 'Tipe',
			'kolom' => 'Kolom',
			'pertanyaan' => 'Pertanyaan',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('tipe',$this->tipe,true);
		$criteria->compare('kolom',$this->kolom,true);
		$criteria->compare('pertanyaan',$this->pertanyaan,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

                                            if(!empty($this->from_date) && empty($this->to_date))
                                           {
                                               $criteria->condition = "created >= '$this->from_date'";
                                           }
                                           else if(!empty ($this->to_date) && empty ($this->from_date))
                                           {
                                               $criteria->condition = "created <= '$this->to_date'";
                                           }
                                           else if(!empty ($this->from_date) && !empty ($this->to_date))
                                           {
                                               $criteria->condition = "created >= '$this->from_date' and created <= '$this->to_date'";
                                           }
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function cboDropDown()
        {
            $role = Yii::app()->user->role;
            switch ($role) {
                case 1:
                    $key = array(' ',1,2,3);
                    $array = array('Semua','Fakultas','Jurusan','Prodi');
                    break;
                case 2:
                    $key = array(' ',1,2,3);
                    $array = array('Semua','Fakultas','Jurusan','Prodi');
                    break;
                case 3:
                    $key = array(' ',1,2,3);
                    $array = array('Semua','Fakultas','Jurusan','Prodi');
                    break;
                //Rektor
                case 4:
                    $key = array('',1,2,3);
                    $array = array('Semua','Fakultas','Jurusan','Prodi');
                    break;
                //Dekan
                case 5:
                    $key = array(1);
                    $array = array('Fakultas');
                    break;
                //Akademik
                case 6:
                    $key = array(3);
                    $array = array('Prodi');
                    break;
                default:
                    $array = array();
                    break;
            }
            $i=0;
            foreach($array as $a) :
                $data.= "<option value='$key[$i]'>".$array[$i]."</option>";
            $i++;
            endforeach;
            return $data;
        }
        
        public function getFilterByRole()
        {
                $model = Admin::model()->findByPk(Yii::app()->user->id);
                $role = Yii::app()->user->role;
                switch ($role) {
                    case 3:
                        $type = 'admin';
                        $values = Yii::app()->user->id;
                        return $type.'="'.$values.'"';
                    case 5:
                        $type = 'fakultas';
                        $values = $model->prodi;
                        return $type.'="'.$values.'"';
                        break;
                    case 6:
                        $type = 'prodi';
                        $values = $model->prodi;
                        return $type.'="'.$values.'"';
                    default:
                        break;
                }
            }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Soal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
