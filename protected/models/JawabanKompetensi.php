<?php

/**
 * This is the model class for table "jawaban_kompetensi".
 *
 * The followings are the available columns in table 'jawaban_kompetensi':
 * @property integer $id
 * @property integer $mahasiswa
 * @property integer $soal
 * @property integer $jawaban_a
 * @property integer $jawaban_b
 *
 * The followings are the available model relations:
 * @property Mahasiswa $mahasiswa0
 * @property Soal $soal0
 */
class JawabanKompetensi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jawaban_kompetensi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mahasiswa, soal, jawaban_a, jawaban_b', 'required'),
			array('mahasiswa, soal, jawaban_a, jawaban_b', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mahasiswa, soal, jawaban_a, jawaban_b', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mahasiswa0' => array(self::BELONGS_TO, 'Mahasiswa', 'mahasiswa'),
			'soal0' => array(self::BELONGS_TO, 'Soal', 'soal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mahasiswa' => 'Mahasiswa',
			'soal' => 'Soal',
			'jawaban_a' => 'Jawaban A',
			'jawaban_b' => 'Jawaban B',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mahasiswa',$this->mahasiswa);
		$criteria->compare('soal',$this->soal);
		$criteria->compare('jawaban_a',$this->jawaban_a);
		$criteria->compare('jawaban_b',$this->jawaban_b);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getStatus($stats)
        {
            switch ($stats) {
                case 1:
                    return "Sangat Kurang";
                    break;
                case 2:
                    return "Kurang";
                    break;
                case 3:
                    return "Cukup";
                    break;
                case 4:
                    return "Baik";
                    break;
                case 5:
                    return "Sangat Baik";
                    break;
                default:
                    break;
            }
        }
  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JawabanKompetensi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
