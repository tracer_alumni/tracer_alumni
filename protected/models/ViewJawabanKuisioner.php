<?php

/**
 * This is the model class for table "view_jawaban_kuisioner".
 *
 * The followings are the available columns in table 'view_jawaban_kuisioner':
 * @property integer $id
 * @property string $pilihan
 * @property string $keterangan
 * @property string $nim
 * @property integer $tahun_masuk
 * @property integer $tahun_lulus
 * @property string $jenis
 * @property integer $prodi
 * @property string $nama_prodi
 * @property integer $jurusan
 * @property string $nama_jurusan
 * @property integer $fakultas
 * @property string $nama_fakultas
 * @property string $nomor
 */
class ViewJawabanKuisioner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'view_jawaban_kuisioner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pilihan, nim, tahun_masuk, tahun_lulus, jenis, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, nomor', 'required'),
			array('id, tahun_masuk, tahun_lulus, prodi, jurusan, fakultas', 'numerical', 'integerOnly'=>true),
			array('pilihan, keterangan', 'length', 'max'=>200),
			array('nim', 'length', 'max'=>12),
			array('jenis', 'length', 'max'=>1),
			array('nama_prodi', 'length', 'max'=>45),
			array('nama_jurusan, nama_fakultas', 'length', 'max'=>100),
			array('nomor', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pilihan, keterangan, nim, tahun_masuk, tahun_lulus, jenis, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, nomor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

                          public function countData($attr)
        {
            $model = ViewJawabanKuisioner::model()->countByAttributes($attr);
            return (int)$model;
        }
        
        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pilihan' => 'Pilihan',
			'keterangan' => 'Keterangan',
			'nim' => 'Nim',
			'tahun_masuk' => 'Tahun Masuk',
			'tahun_lulus' => 'Tahun Lulus',
			'jenis' => 'Jenis',
			'prodi' => 'Prodi',
			'nama_prodi' => 'Nama Prodi',
			'jurusan' => 'Jurusan',
			'nama_jurusan' => 'Nama Jurusan',
			'fakultas' => 'Fakultas',
			'nama_fakultas' => 'Nama Fakultas',
			'nomor' => 'Nomor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pilihan',$this->pilihan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('tahun_masuk',$this->tahun_masuk);
		$criteria->compare('tahun_lulus',$this->tahun_lulus);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('prodi',$this->prodi);
		$criteria->compare('nama_prodi',$this->nama_prodi,true);
		$criteria->compare('jurusan',$this->jurusan);
		$criteria->compare('nama_jurusan',$this->nama_jurusan,true);
		$criteria->compare('fakultas',$this->fakultas);
		$criteria->compare('nama_fakultas',$this->nama_fakultas,true);
		$criteria->compare('nomor',$this->nomor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewJawabanKuisioner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
