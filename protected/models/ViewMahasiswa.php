<?php

/**
 * This is the model class for table "view_mahasiswa".
 *
 * The followings are the available columns in table 'view_mahasiswa':
 * @property integer $id
 * @property string $nim
 * @property string $nama
 * @property integer $prodi
 * @property string $nama_prodi
 * @property integer $jurusan
 * @property string $nama_jurusan
 * @property integer $fakultas
 * @property string $nama_fakultas
 * @property integer $tahun_masuk
 * @property integer $tahun_lulus
 * @property integer $admin
 * @property string $created
 */
class ViewMahasiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'view_mahasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, nama, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, tahun_masuk, tahun_lulus, created', 'required'),
			array('id, prodi, jurusan, fakultas, tahun_masuk, tahun_lulus, admin', 'numerical', 'integerOnly'=>true),
			array('nim', 'length', 'max'=>12),
			array('nama, nama_prodi', 'length', 'max'=>45),
			array('nama_jurusan, nama_fakultas', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nim, nama, prodi, nama_prodi, jurusan, nama_jurusan, fakultas, nama_fakultas, tahun_masuk, tahun_lulus, admin, created', 'safe', 'on'=>'search'),
		);
	}

        public function getPrimaryKey() {
            return $this->id;
        }
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nim' => 'Nim',
			'nama' => 'Nama',
			'prodi' => 'Prodi',
			'nama_prodi' => 'Nama Prodi',
			'jurusan' => 'Jurusan',
			'nama_jurusan' => 'Nama Jurusan',
			'fakultas' => 'Fakultas',
			'nama_fakultas' => 'Nama Fakultas',
			'tahun_masuk' => 'Tahun Masuk',
			'tahun_lulus' => 'Tahun Lulus',
			'admin' => 'Admin',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('prodi',$this->prodi);
		$criteria->compare('nama_prodi',$this->nama_prodi,true);
		$criteria->compare('jurusan',$this->jurusan);
		$criteria->compare('nama_jurusan',$this->nama_jurusan,true);
		$criteria->compare('fakultas',$this->fakultas);
		$criteria->compare('nama_fakultas',$this->nama_fakultas,true);
		$criteria->compare('tahun_masuk',$this->tahun_masuk);
		$criteria->compare('tahun_lulus',$this->tahun_lulus);
		$criteria->compare('admin',$this->admin);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewMahasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
