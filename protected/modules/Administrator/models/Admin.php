<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $id
 * @property string $nama_lengkap
 * @property string $gelar
 * @property string $nama_akun
 * @property string $katasandi
 * @property integer $status
 * @property integer $role
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Mahasiswa[] $mahasiswas
 */
class Admin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lengkap, nama_akun, katasandi, created, modified', 'required'),
			array('status, role, prodi', 'numerical', 'integerOnly'=>true),
			array('nama_lengkap', 'length', 'max'=>45),
			array('gelar, nama_akun', 'length', 'max'=>12),
			array('katasandi', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_lengkap, gelar, nama_akun, katasandi, status, role, prodi, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                                                 'prodi0' => array(self::BELONGS_TO, 'Prodi', 'prodi'),
			'mahasiswas' => array(self::HAS_MANY, 'Mahasiswa', 'admin'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_lengkap' => 'Nama Lengkap',
			'gelar' => 'Gelar',
			'nama_akun' => 'Nama Akun',
			'katasandi' => 'Katasandi',
			'status' => 'Status',
			'role' => 'Role',
                                                                'prodi' => 'Prodi',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}
        
        protected function beforeSave() {
            parent::beforeSave();
            $this->katasandi = md5($this->katasandi);
            return true;
        }
        
        public function getAdmin($id)
        {
            $model = Admin::model()->findByPk($id);
            if($model == NULL)
            {
                return 'Mandiri';
            }
            else
            {
                return $model->nama_lengkap;
            }
        }
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('gelar',$this->gelar,true);
		$criteria->compare('nama_akun',$this->nama_akun,true);
		$criteria->compare('katasandi',$this->katasandi,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('role',$this->role);
                                           $criteria->compare('prodi',$this->prodi);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function cboListRole()
        {
            return array(
                '2' => 'Administrator',
                '3' => 'Surveyor',
                '4' => 'Rektor',
                '5' => 'Dekan',
                '6' => 'Akademik',
            );
        }
        
        public function getRole($role)
        {
            switch ($role) {
                case 1:
                    return 'Super Administrator';
                    break;
                case 2:
                    return 'Administrator';
                    break;
                case 3:
                    return 'Surveyor';
                    break;
                case 4:
                    return 'Academic';
                    break;
                case 5:
                    return 'Dekan';
                case 6 :
                    return 'Rektor';
                default:
                    return 'Surveyor';
                    break;
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
