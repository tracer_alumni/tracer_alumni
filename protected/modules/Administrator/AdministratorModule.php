<?php

class AdministratorModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
                  Yii::app()->setComponents(array(
                        'errorHandler'=>array(
                                'errorAction'=>'Administrator/default/error',
                        ),
                ));
            
		$this->setImport(array(
			'Administrator.models.*',
			'Administrator.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
                    Yii::app()->user->loginUrl=array('/Administrator/Default/Index');
			return true;
		}
		else
			return false;
	}
}
