<?php

class HeaderController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index','countCollege','countDataSurveyor'),
                'expression' => 'Yii::app()->user->role <= 4',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    //Default index action for loading the notification
    public function actionIndex($view = 'index')
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $now = date('Y-m-d');
            
            $sql_model = "SELECT id,nim,nama,created FROM mahasiswa WHERE created  LIKE '%".$now."%' ORDER BY created DESC LIMIT 5";
             $sql_total = "SELECT COUNT(id) FROM mahasiswa WHERE created LIKE '%".$now."%' ";
            
            if(Yii::app()->user->role == 3)
            {
                $user_id = Yii::app()->user->role;
                $sql_model = "SELECT id,nim,nama,created FROM mahasiswa WHERE created  LIKE '%".$now."%' AND admin = '$user->_id' ORDER BY created DESC LIMIT 5";
                $sql_total = "SELECT COUNT(id) FROM mahasiswa WHERE created LIKE '%".$now."%'  AND admin = '$user->_id'";
            }
            else if(Yii::app()->user->role == 4) 
            {
                $user = Admin::model()->findByPk(Yii::app()->user->id);
                $prodi_id = Prodi::model()->findByPk($user->prodi)->id;
                $sql_model = "SELECT id,nim,nama,created FROM mahasiswa WHERE created  LIKE '%".$now."%' AND prodi = '$prodi_id' ORDER BY created DESC LIMIT 5";
                $sql_total = "SELECT COUNT(id) FROM mahasiswa WHERE created LIKE '%".$now."%'  AND prodi = '$prodi_id'";
            }
             $model = Yii::app()->db->createCommand($sql_model)->queryAll();
             $total = Yii::app()->db->createCommand($sql_total)->queryScalar();
             $this->renderPartial($view,array(
                 'model' => $model,
                 'total' => $total,
                 'periods' => $this->periods,
                 'lengths' => $this->lengths,
              ));
             }
    }
    
    public function actionCountCollege()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $now = date('Y-m-d');
            $sql_mhs = "SELECT COUNT(id) FROM mahasiswa";
            $sql_mhs_today = "SELECT COUNT(id) FROM mahasiswa WHERE created LIKE '%".$now."%' ";
            $sql_mhs_admin = "SELECT COUNT(id) FROM mahasiswa WHERE created LIKE '%".$now."%' AND admin >= 1";
            $json = array(
                'mahasiswa_total' =>Yii::app()->db->createCommand($sql_mhs)->queryScalar(),
                'mahasiswa_today' => Yii::app()->db->createCommand($sql_mhs_today)->queryScalar(),
                'mahasiswa_surveyor' => Yii::app()->db->createCommand($sql_mhs_admin)->queryScalar(),
            );
            echo CJSON::encode($json);
        }
    }
    
    public function actionCountDataSurveyor()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $sql = "SELECT id,nama_lengkap,gelar,role FROM admin WHERE role = 3 ORDER BY nama_lengkap ASC";
            $sql_count = "SELECT COUNT(id) AS nilai FROM mahasiswa";
            $model = Yii::app()->db->createCommand($sql)->queryAll();
            $this->renderPartial('surveyor_entry',array(
                'model' => $model,
                'count' =>$sql_count,
             ));
        }
    }
}