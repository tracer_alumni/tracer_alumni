<?php

class JurusanController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('create','delete','update','index','view'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actions() {
        return array(
            'index' => array(
                'class' => 'ListDataAction',
                'sort' => array('defaultOrder' => array('nama' => CSort::SORT_ASC)),
                'view' => '/jurusan/list',
            ),
            'create' => array(
                'class' => 'CreateAction',
                'modelClass' => 'Jurusan',
                'redirectTo' => '/index',
                'formClass' => '.index',
            ),
            'update' => array(
                'class' => 'UpdateAction',
                'redirectTo' => '/index',
                'formClass' =>'.index',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
                'redirectTo' => '/index',
            ),
        );
    }  
}