<?php

class DefaultController extends MController
{ 
    public function actionIndex()
    {
        if(!Yii::app()->user->isGuest)
        {
            $this->redirect($this->requestBaseUrl().'Mahasiswa');
        }
        $model = new LoginForm;
        $this->performAjaxValidation($model,'admin-login-form');
        
        if(isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];
            if($model->login())
            {
                if(Yii::app()->user->role >= 3)
                {
                   $this->redirect($this->requestBaseUrl().'mahasiswa/index');
                }
                else
                {
                    $this->redirect($this->requestBaseUrl().'Dashboard');
                }
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Nama akun atau katasandi salah');
            }
        }
        
        $this->render('login',array('model' => $model));
    }
    
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect($this->requestBaseUrl().'default');
    }
    
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
         {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
            }
     }
}