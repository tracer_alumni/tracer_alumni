<?php

class A
{

private function Render2Chart1($id)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/exporting.js',  CClientScript::POS_END);
        $title = Soal::model()->findByPk($id)->pertanyaan;
        $yTitle = 'Jumlah';
        $x = array();
        $data = array();
        $data2 = array();
         $sql = "
             SELECT  jawaban_kompetensi.jawaban_a , jawaban_kompetensi.jawaban_b
             FROM jawaban_kompetensi
             LEFT JOIN soal ON soal.id = jawaban_kompetensi.soal 
             LEFT JOIN mahasiswa ON mahasiswa.id = jawaban_kompetensi.mahasiswa
             WHERE jawaban_kompetensi.soal = '$id'";
         $sql .= ";";
         if(isset(Yii::app()->session['query']))
         {
             $s = " WHERE jawaban_kompetensi.soal = '$id' ";
             $sql = Yii::app()->session['query'].$s.Yii::app()->session['query_end'];
         }
         $xTitle = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($xTitle as $row) :
            $data[] = $row['jawaban_a'];
            $data2[] = $row['jawaban_b'];
        endforeach;
        $this->render('chart_2',array(
            'title' => $title,
            'yTitle' => $yTitle,
            'xAxis' => $x,
            'data' => $this->countValue($data),
            'data2' => $this->countValue($data2),
        ));
    }

 public function actionrenderOther2($id)
    {
         if(Yii::app()->request->isAjaxRequest) {
            if(empty($id))
            {
                throw new CHttpException(404);
            }
         $sql = "SELECT * FROM pilihan WHERE soal_id = '$id' ORDER BY pilihan ASC;";
         $model = Yii::app()->db->createCommand($sql)->queryAll();
         $sql_answer = "
                 SELECT jawaban.id,jawaban.mahasiswa,jawaban.pilihan,jawaban.keterangan 
                 FROM jawaban
                 LEFT JOIN pilihan ON pilihan.id = jawaban.pilihan 
                 LEFT JOIN mahasiswa ON mahasiswa.id = jawaban.mahasiswa
                 LEFT JOIN prodi ON prodi.id = mahasiswa.prodi
                 LEFT JOIN jurusan ON jurusan.id = prodi.jurusan
                 LEFT JOIN fakultas ON fakultas.id = jurusan.fakultas
                 ";
         $this->renderPartial('other_column',array(
             'model' => $model,
             'sql_answer' => $sql_answer,
          ));
        }
    }
    
    public function actionFilterAnswer2()
    {
        unset(Yii::app()->session['query']);
        unset(Yii::app()->session['query_end']);
        unset(Yii::app()->session['filter_display']);
        unset(Yii::app()->session['query_end_column']);
        if(isset($_POST['Mahasiswa']))
        {
            if(Yii::app()->user->role == 4) 
            {
                $pilih = 4;
                $prodi = Admin::model()->findByPk(Yii::app()->user->id)->prodi;
            }
            else
            {
                $pilih = $_POST['Mahasiswa']['pilih'];
                $prodi = $_POST['Mahasiswa']['prodi'];
            }
            $thn_msk = $_POST['Mahasiswa']['tahun_masuk'];
            $thn_lls = $_POST['Mahasiswa']['tahun_lulus'];
            $jenis = $_POST['Mahasiswa']['jenis'];
            $id = $_POST['Soal']['id'];
            $soal = Soal::model()->findByPk($id)->nomor;
            $j;
            if($soal <= 3)
            {
                  $query = "
                     SELECT COUNT(jawaban.id) AS total FROM jawaban
                     LEFT JOIN mahasiswa ON mahasiswa.id = jawaban.mahasiswa 
                     LEFT JOIN prodi ON prodi.id = mahasiswa.prodi
                     LEFT JOIN jurusan ON jurusan.id = prodi.jurusan
                     LEFT JOIN fakultas ON fakultas.id = jurusan.fakultas ";
                  
                switch ($pilih) {
                    case 2:
                        $type = 'fakultas.id';
                        $t = 'Fakultas';
                        break;
                    case 3:
                        $type = 'jurusan.id';
                        $t = 'Jurusan';
                        break;
                    case 4:
                        $type = 'prodi.id';
                        $t = 'Prodi';
                        break;
                    default:
                        $type = 'fakultas.id';
                        $t = 'Fakultas';
                        break;
                }
                $query_end = "AND $type = '$prodi' AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                if($pilih == 1)
                {
                    $query_end = "AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                }
                if($jenis != NULL)
                {
                    $query_end .= "AND mahasiswa.jenis = '$jenis' ";
                    if($jenis == 'L') { $j = 'Dan Jenis Kelamin Laki laki'; } else { $j = 'Dan Jenis Kelamin Perempuan'; }
                 }
                 $query_end .= ";";
                 
                 $p = $t::model()->findByPk($prodi);
                 $filter_display = 'Berdasarkan '.$t.' '.$p->nama.' , Tahun Masuk'.$thn_msk.' , Tahun Lulus '.$thn_lls.' '.$j;
            }
            else
            {
                $query = " 
                    SELECT  jawaban_kompetensi.jawaban_a , jawaban_kompetensi.jawaban_b 
                    FROM jawaban_kompetensi 
                    LEFT JOIN soal ON soal.id = jawaban_kompetensi.soal
                    LEFT JOIN mahasiswa ON mahasiswa.id = jawaban_kompetensi.mahasiswa 
                     LEFT JOIN prodi ON prodi.id = mahasiswa.prodi
                     LEFT JOIN jurusan ON jurusan.id = prodi.jurusan
                     LEFT JOIN fakultas ON fakultas.id = jurusan.fakultas ";
                switch ($pilih) {
                    case 2:
                        $type = 'fakultas.id';
                        $t = 'Fakultas';
                        break;
                    case 3:
                        $type = 'jurusan.id';
                        $t = 'Jurusan';
                        break;
                    case 4:
                        $type = 'prodi.id';
                        $t = 'Prodi';
                        break;
                    default:
                        $type = 'fakultas.id';
                        $t = 'Fakultas';
                        break;
                }
                $query_end = "AND $type = '$prodi' AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                if($pilih == 1)
                {
                    $query_end = "AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                }
                 if($jenis != NULL)
                {
                    $query_end .= "AND mahasiswa.jenis = '$jenis' ";
                        if($jenis == 'L') { $j = 'Dan Jenis Kelamin Laki laki'; } else { $j = 'Dan Jenis Kelamin Perempuan'; }
                 }
                 $query_end .= ";";
                 $p = $t::model()->findByPk($prodi);
                 $filter_display = 'Berdasarkan '.$t.' '.$p->nama.' , Tahun Masuk'.$thn_msk.' , Tahun Lulus '.$thn_lls.' '.$j;
            }
            
            //For render the other column such as Keterangan
            if($soal <= 3)
            {
                   switch ($pilih) {
                    case 2:
                        $type = 'fakultas.id';
                        break;
                    case 3:
                        $type = 'jurusan.id';
                        break;
                    case 4:
                        $type = 'prodi.id';
                        break;
                    default:
                        $type = 'fakultas.id';
                        break;
                }
                $query_end_column = "AND $type = '$prodi' AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                if($pilih == 1)
                {
                    $query_end_column = "AND mahasiswa.tahun_masuk >= '$thn_msk' AND mahasiswa.tahun_lulus <= '$thn_lls' ";
                }
                if($jenis != NULL)
                {
                    $query_end_column .= "AND mahasiswa.jenis = '$jenis' ";
                 }
            }
            
            Yii::app()->session['query'] = $query;
            Yii::app()->session['query_end'] = $query_end;
            Yii::app()->session['query_end_column'] = $query_end_column;
            Yii::app()->session['filter_display'] = $filter_display;
            $this->redirect(array('soal/chart/id/'.$id));
        }
        else
        {
            $this->renderPartial('filter');
        }
    }
}