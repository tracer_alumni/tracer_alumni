<?php

class AdminController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('changePassword'),
                'expression' => 'Yii::app()->user->role <=4',
            ),
            array('allow',
                'actions' => array('index','create','delete','update','view','loadType'),
                'expression' => 'Yii::app()->user->role <= 1',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actions() {
        return array(
            'index' => array(
                'class' => 'ListDataAction',
                'criteria' => array('condition' => 'role >= 2','order' => 'nama_lengkap ASC'),
                'view' => '/admin/list',
            ),
            'update' => array(
                'class' => 'UpdateAction',
                'redirectTo' => '/index',
                'formClass' =>'.index',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
                'redirectTo' => '/index',
            ),
            'view' => array(
                'class' => 'ViewAction',
                'view' => 'view',
            ),
        );
    }
    
    public function actionchangePassword()
    {
        $model = Admin::model()->findByPk(Yii::app()->user->id);
        if(isset($_POST['Admin']))
        {
            $old = md5($_POST['Admin']['katasandi_lama']);
            $new = md5($_POST['Admin']['katasandi_baru']);
            $confirm = md5($_POST['Admin']['konfirmasi_katasandi']);
            
            if($old == $model->katasandi)
            {
                if($new == $confirm)
                {
                    $model->katasandi = $_POST['Admin']['katasandi_baru'];
                    if($model->update())
                    {
                        Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Katasandi berhasil diubah');
                        $this->redirect(array('/Administrator/adminview/id/'.Yii::app()->user->id));
                    }
                    else
                    {
                        Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Katasandi gagal untuk diubah');
                    }
                }
                else
                {
                    Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Katasandi baru tidak cocok dengan konfirmasi');
                }
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Katasandi sebelumnya tidak cocok dengan sebelumnya');
            }
        }
        
        $this->render('editAccount',array('model' => $model));
    }
    
    public function actionCreate()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/dependent_dropdown_admin.js',  CClientScript::POS_END);
        $model = new Admin;
        $form = new CForm('application.modules.Administrator.views.admin.index',$model);
        $this->performAjaxValidation($model,'form-id');
        if($form->submitted('Click'))
        {
            $model->attributes = $_POST['Admin'];
            $model->prodi = NULL;
            if($_POST['Admin']['role'] >= 5)
            {
                if($_POST['Admin']['prodi'] != NULL)
                {
                    $model->prodi = $_POST['Admin']['prodi'];
                }
                else
                {
                    Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Mohon diperiksa kembali ! Ada data kosong');
                    $this->refresh();
                }
            }
            $model->created = $this->getCurrentTime();
            $model->modified = $this->getCurrentTime();
            if($model->save())
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Data berhasil dimasukkan');
                $this->redirect(array('index'));
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Data gagal untuk dimasukkan');
                $this->refresh();
            }
        }
        $this->render('/layouts/form',array('form' => $form,'model' => $model));
    }
    
    public function actionLoadType($id)
     {
         if(Yii::app()->request->isAjaxRequest) {
             switch ($id) {
             case 5:
                 $model = Fakultas::model()->findAll(array('order' => 'nama_fakultas ASC'));
                 break;
             case 6:
                 $model = Prodi::model()->findAll(array('order' => 'nama_prodi ASC'));
                 break;
             default:
                 $model = Fakultas::model()->findAllByAttributes(array('id' => '10000'));
                 break;
            }
         $this->renderPartial('filter_dropdown',array('model' => $model,'id' => $id));
         }
     }
}