<?php

class DashboardController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }  
    
    public function actionIndex()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/dashboard.js',  CClientScript::POS_END);
        $this->render('index');
    }
}