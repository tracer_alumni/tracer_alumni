<?php

class FakultasController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('create','delete','update','index','view'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actions() {
        return array(
            'index' => array(
                'class' => 'ListDataAction',
                'sort' => array('defaultOrder' => array('nama' => CSort::SORT_ASC)),
                'view' => '/fakultas/list',
            ),
            'create' => array(
                'class' => 'CreateAction',
                'redirectTo' => '/index',
                'formClass' => '.index',
            ),
            'update' => array(
                'class' => 'UpdateAction',
                'redirectTo' => '/index',
                'formClass' =>'.index',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
                'redirectTo' => '/index',
            ),
            'view' => array(
                'class' => 'ViewAction',
                'view' => 'view',
            ),
        );
    }  
}