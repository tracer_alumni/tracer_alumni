<?php

class MahasiswaController extends MController
{    
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index','answer','search','view'),
                'expression' => 'Yii::app()->user->role >= 1'
            ),
            array('allow',
                'actions' => array('delete','update'),
                'expression' => 'Yii::app()->user->role == 1',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actions() {
        return array(
            'update' => array(
                'class' => 'UpdateAction',
                'redirectTo' => '/index',
                'formClass' =>'.update',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
                'redirectTo' => '/index',
            ),
            'view' => array(
                'class' => 'ViewAction',
                'view' => 'view',
            ),
        );
    }
    
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('ViewMahasiswa',array(
            'criteria' => array(
                'condition' => $this->getDataByRole(),
            ),
        ));
        $this->render('list',array('dataProvider' => $dataProvider));
    }
    
    public function actionAnswer($id)
    {
        if(empty($_GET['id']))
        {
            throw  new CHttpException(404);
        }
        $soal = Soal::model()->findAll(array('order' => 'nomor ASC','condition' => 'nomor < 4'));
        $soal4 = Soal::model()->findAll(array('order' => 'nomor ASC','condition' => 'nomor >= 4'));
        $saran = Saran::model()->findByAttributes(array('mahasiswa' => $id));
        if($saran == NULL)
        {
            $saran = new Saran();
        }
        $jawaban = Jawaban::model()->findAllByAttributes(array('mahasiswa' => $id));
        $this->render('answer',array(
            'soal' => $soal,
            'soal4' => $soal4,
            'saran' => $saran->saran,
            'jawaban' => $jawaban,
            'mahasiswa_id' => $id,
         ));
    }
    
    public function actionSearch()
    {
        unset(Yii::app()->request->cookies['from_date']);
        unset(Yii::app()->request->cookies['to_date']);
        $model = new Mahasiswa('search');
        $model->unsetAttributes();
        if(isset($_POST['Mahasiswa']))
        {
            Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date',$_POST['Mahasiswa']['from_date']);
            Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date',$_POST['Mahasiswa']['to_date']);
            $model->attributes = $_POST['Mahasiswa'];
            $model->from_date = $_POST['Mahasiswa']['from_date'];
            $model->to_date = $_POST['Mahasiswa']['to_date'];
            $this->render('list',array('dataProvider' => $model->search()));
        }
        else
        {
            $this->render('search',array('model' => $model));
        }
    }
    
    public function getDataByRole()
    {
        $role = Yii::app()->user->role;
        switch ($role) {
            case 3:
                $condition = 'admin = '.Yii::app()->user->id;
                break;
            case 5:
                $model = Admin::model()->findByPk(Yii::app()->user->id);
                $condition = 'fakultas = '.$model->prodi;
                break;
            case 6:
                $model = Admin::model()->findByPk(Yii::app()->user->id);
                $condition = 'prodi = '.$model->prodi;
                break;
            default:
                $condition = '';
                break;
        }
        return $condition;
    }
}