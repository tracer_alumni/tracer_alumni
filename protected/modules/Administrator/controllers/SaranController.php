<?php

class SaranController extends MController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index'),
                'expression' => 'Yii::app()->user->role <= 6',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex()
    {
        $condition = array(
            'criteria' => array(
                'condition' => Soal::model()->getFilterByRole(),
            ),
        );
        $model = new CActiveDataProvider('ViewSaran',$condition);
        $this->render('list',array('dataProvider' => $model));
    }
}