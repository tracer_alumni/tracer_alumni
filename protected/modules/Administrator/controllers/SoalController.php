<?php

class SoalController extends MController
{   
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index','view','chart','search','renderOther','filterAnswer','filterOff','loadType','exportXLS','exportXLS2'),
                'expression' => 'Yii::app()->user->role <= 9 ',
            ),
            array('allow',
                'actions' => array('create','delete','update','deleteChoice','editChoice'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actions() {
        return array(
            'create' => array(
                'class' => 'CreateAction',
                'redirectTo' => '/index',
                'formClass' => '.index',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
                'redirectTo' => '/index',
            ),
            'update' => array(
                'class' => 'UpdateAction',
                'redirectTo' => '/index',
                'formClass' => '.index'
            ),
            'deleteChoice' => array(
                'class' => 'DeleteAction',
                'modelClass' => 'Pilihan',
                'redirectTo' => '/index',
            ),
             'search' => array(
                'class' => 'SearchAction',
                'column' => 'pertanyaan',
                'view' => 'list',
            ),
        );
    }  
    
    public function actionIndex()
    {
        unset(Yii::app()->session['filter']);
        $dataProvider = new CActiveDataProvider('Soal',array(
            'criteria' => array(
                'order'=> 'nomor asc',
            ),
            'pagination' => array(
                'pageSize' =>50,
            ),
        ));
        $this->render('list',array('dataProvider' => $dataProvider));
    }
    
    public function actionView($id)
    {
        $this->checkParam('id');
        $model = Soal::model()->findByPk($id);
        $new_answer = new Pilihan;
        $this->performAjaxValidation($new_answer,'form-id');
        if(isset($_POST['Pilihan']))
        {
            $new_answer->attributes=$_POST['Pilihan'];
            $new_answer->soal_id = $model->id;
            if($new_answer->save())
            {
                $this->redirect($this->requestBaseUrl().'soal/view/id/'.$id);
            }
        }
        $this->render('view',array('model' => $model,'new_answer' => $new_answer));
    }
    
     public function actionEditChoice($id)
    {
        $this->checkParam('id');
        $model = Pilihan::model()->findByPk($id);
        $this->performAjaxValidation($model,'form-id');
        if(isset($_POST['Pilihan']))
        {
            $model->attributes=$_POST['Pilihan'];
            $model->soal_id = $model->soal->id;
            if($model->update())
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Data berhasil diubah');
                $this->redirect($this->requestBaseUrl().'soal/view/id/'.$model->soal->id);
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Data gagal dibuat');
            }
        }
        $this->render('editChoice',array('model' => $model));
    }
    
    public function actionChart($id)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/exporting.js',  CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/dependent_dropdown.js',  CClientScript::POS_END);
        unset(Yii::app()->session['soal_id']);
        if(empty($_GET['id']))
        {
            throw new CHttpException(404);
        }
        else
        {
            Yii::app()->session['soal_id'] = $_GET['id'];
        }
        $model = Soal::model()->findByPk($id);
        if($model == NULL)
        {
            throw new CHttpException(404);
        }
        if($model->nomor < 4.0)
        {
            $this->RenderChart($id);
        }
        else
        {
            $this->Render2Chart($id);
        }
    }
    
    private function RenderChart($id)
    {
        if($id == NULL)
        {
            throw new CHttpException(404);
        }
        $soal = Soal::model()->findByPk($id);
        if($soal == NULL)
        {
            throw new CHttpException(404);
        }
        $x = array();
        $data = array();
        $attr = array(
            'nomor' => $soal->nomor,
        );
        if(isset(Yii::app()->session['filter']))
        {
            $condition = Yii::app()->session['filter'];
            echo CJSON::encode($condition);
        }
        else
        {
            $condition = array(
                'condition' => Soal::model()->getFilterByRole(),
            );
            echo 'Filter Off : '.CJSON::encode($condition);
        }
        $model = ViewJawabanKuisioner::model()->findAllByAttributes($attr,$condition);
        $pilihan = Pilihan::model()->findAllByAttributes(array('soal_id' => $soal->id));
        foreach ($model as $row) :
                $c = ViewJawabanKuisioner::model()->countData(array('pilihan' => $row->pilihan));
                $data[] = array('name' => $row->pilihan,'data' => array($c));
        endforeach;
        $this->render('chart',array(
            'title' => $soal->pertanyaan,
            'yTitle' => 'Jumlah',
            'xAxis' => $x,
            'data' => $data,
        ));
    }
    
    private function RenderChart4($id)
    {
        $soal_model = Soal::model()->findByPk($id);
        $x_sql = "SELECT pilihan.id , pilihan.pilihan FROM pilihan WHERE soal_id = '$soal_model->id' ";
        $xAxis = Yii::app()->db->createCommand($x_sql)->queryAll();
        $x = array();
        $data = array();
        foreach ($xAxis as $row):
            $id = $row['id'];
            $s = "WHERE jawaban.pilihan = '$id' ";
            $sql = isset(Yii::app()->session['query']) ? Yii::app()->session['query'].$s.Yii::app()->session['query_end'] :  "SELECT COUNT(id) AS total FROM jawaban WHERE pilihan = '$id' ";
            if(Yii::app()->user->role == 3)
            {
                $user_id = Yii::app()->user->id;
                $s = "WHERE jawaban.pilihan ='$id' mahasiswa.admin= '$user_id' ";
            }
            $count = Yii::app()->db->createCommand($sql)->queryScalar();
            $c = (int)$count;
            $data[] = array('name' => $row['pilihan'],'data' => array($c));
        endforeach;
        $this->render('chart',array(
            'title' => $soal_model->pertanyaan,
            'yTitle' => 'Jumlah',
            'xAxis' => $x,
            'data' => $data,
        ));
    }
    
    private function Render2Chart($id)
    {
        if($id == NULL)
        {
            throw new CHttpException(404);
        }
        $soal = Soal::model()->findByPk($id);
        if($soal == NULL)
        {
            throw new CHttpException(404);
        }
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/exporting.js',  CClientScript::POS_END);
        $x = array();
        $data = array();
        $data2 = array();
        $attr = array(
            'nomor' => $soal->nomor,
        );
        if(isset(Yii::app()->session['filter']))
        {
            $condition = Yii::app()->session['filter'];
            echo CJSON::encode($condition);
        }
        else
        {
            $condition = array(
                'condition' => Soal::model()->getFilterByRole(),
            );
            echo 'Filter Off : '.CJSON::encode($condition);
        }
        $model = ViewJawabanKompetensi::model()->findAllByAttributes($attr,$condition);
        foreach($model as $row) :
            $data[] = $row->jawaban_a;
            $data2[] = $row->jawaban_b;
        endforeach;
        $this->render('chart_2',array(
            'title' => $soal->pertanyaan,
            'yTitle' => 'Jumlah',
            'xAxis' => $x,
            'data' => $this->countValue($data),
            'data2' => $this->countValue($data2),
        ));
    }
    
    public function actionRenderOther($id)
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            if($id == NULL)
            {
                throw new CHttpException(404);
            }
            $soal = Soal::model()->findByPk($id);
            if($soal == NULL)
            {
                throw new CHttpException(404);
            }
            $attr = array(
                'nomor' => $soal->nomor,
            );
            if(isset(Yii::app()->session['filter']))
            {
                $condition = Yii::app()->session['filter'];
            }
            else
            {
                $condition = array(
                    'condition' => Soal::model()->getFilterByRole(),
                );
            }
            $model = ViewJawabanKuisioner::model()->findAllByAttributes($attr,$condition);
            $this->renderPartial('other_column',array('model' => $model));
        }
    }
    
    private function countValue($data)
    {
        $i1 = 0;
        $i2 = 0;
        $i3 = 0;
        $i4 = 0;
        $i5 = 0;
        $length = count($data);
        for($i=0;$i<$length;$i++)
        {
            switch ($data[$i]) {
                case 1:
                    $i1++;
                    break;
                case 2 :
                    $i2++;
                    break;
                case 3 :
                    $i3++;
                    break;
                case 4 :
                    $i4++;
                    break;
                case 5 :
                    $i5++;
                    break;
            }
        }
        $value = array(
            array(
                'name' => '1',
                'data' => array($i1),
            ),
            array(
                'name' => '2',
                'data' => array($i2),
            ),
            array(
                'name' => '3',
                'data' => array($i3),
            ),
            array(
                'name' => '4',
                'data' => array($i4),
            ),
            array(
                'name' => '5',
                'data' => array($i5),
            ),
        );
        return $value;
    }
    
    public function actionSearch()
    {
        $model = new Soal('search');
        $model->unsetAttributes();
        if(isset($_POST['Soal']))
        {
            $model->attributes = $_POST['Soal'];
            $this->render('list',array('dataProvider' => $model->search()));
        }
        else
        {
            $this->render('search',array('model' => $model));
        }
    }
    
    public function actionFilterAnswer()
    {
        unset(Yii::app()->session['filter']);
        if(isset($_POST['Mahasiswa']))
        {
            $pilih = $_POST['Mahasiswa']['pilih'];
            $prodi = $_POST['Mahasiswa']['prodi'];
            $thn_msk = $_POST['Mahasiswa']['tahun_masuk'];
            $thn_lls = $_POST['Mahasiswa']['tahun_lulus'];
            $jenis = $_POST['Mahasiswa']['jenis'];
            $soal_id = $_POST['Soal']['id'];
                $this->filterAnswerNormal($pilih,$prodi,$thn_msk,$thn_lls,$jenis,$soal_id);
            
            $this->redirect(array('soal/chart/id/'.$soal_id));
        }
        else
        {
            $this->renderPartial('filter');
        }
    }
    
    public function actionfilterOff($id)
    {
        if($id == NULL)
        {
            $this->redirect(array('/Administrator/soal'));
        }
        else
        {
            unset(Yii::app()->session['filter']);
            $this->redirect(array('/Administrator/soal/chart/id/'.$id));
        }
     }
    
    private function filterAnswerNormal($pilih,$prodi,$thn_msk,$thn_lls,$jenis,$soal_id)
    {
        $soal = Soal::model()->findByPk($soal_id);
        $type = '';
        $value = '';
        switch ($pilih)
        {
            case 1 :
                $type = 'fakultas = ';
                $values = Fakultas::model()->findByPk($prodi)->id;
                $value = '"'.$values.'"';
                break;
            case 2:
                $type = 'jurusan = ';
                $values = Jurusan::model()->findByPk($prodi)->id;
                $value = '"'.$values.'"';
                break;
            case 3:
                $type = 'prodi = ';
                $values = Prodi::model()->findByPk($prodi)->id;
                $value = '"'.$values.'"';
                break;
            default :
                $type = 'id >= ';
                $values = '1';
                $value = '"'.$values.'"';
                break;
        }
        
        if(Yii::app()->user->role >=5)
        {
            $data = ' AND '.Soal::model()->getFilterByRole();
        }
        else
        {
            $data = ' AND '.$type.$value;
        }
        
        if($jenis != NULL)
        {
            $attr = array('condition' => 'tahun_masuk >= '.$thn_msk.' AND tahun_lulus <= '.$thn_lls.' AND jenis = "'.$jenis.'"'.$data,);
        }
        else
        {
            $attr = array('condition' => 'tahun_masuk >= '.$thn_msk.' AND tahun_lulus <= '.$thn_lls.$data,);
        }
        Yii::app()->session['filter' ] = $attr;
     }
    
    
     public function actionLoadType($id)
     {
         if(Yii::app()->request->isAjaxRequest) {
             switch ($id) {
             case 1:
                 $model = Fakultas::model()->findAll(array('order' => 'nama_fakultas ASC'));
                 break;
             case 2:
                 $model = Jurusan::model()->findAll(array('order' => 'nama_jurusan ASC'));
                 break;
             case 3:
                 $model = Prodi::model()->findAll(array('order' => 'nama_prodi ASC'));
                 break;
             default:
                 $model = Fakultas::model()->findAll(array('order' => 'nama_fakultas ASC'));
                 break;
         }
         $this->renderPartial('filter_dropdown',array('model' => $model,'id' => $id));
     }
     }
     
     public function actionExportXLS($id)
     {
         if(empty($_GET['id']))
         {
             throw new CHttpException(404);
         }
         $soal_model = Soal::model()->findByPk($id);
        $x_sql = "SELECT pilihan.id , pilihan.pilihan FROM pilihan WHERE soal_id = '$soal_model->id' ";
        $xAxis = Yii::app()->db->createCommand($x_sql)->queryAll();
        $x = array();
        $data = array();
        foreach ($xAxis as $row):
            $id = $row['id'];
            $s = "WHERE jawaban.pilihan = '$id' ";
            $sql = isset(Yii::app()->session['query']) ? Yii::app()->session['query'].$s.Yii::app()->session['query_end'] :  "SELECT COUNT(id) AS total FROM jawaban WHERE pilihan = '$id' ";
            if(Yii::app()->user->role == 3)
            {
                $user_id = Yii::app()->user->id;
                $s = "WHERE jawaban.pilihan ='$id' mahasiswa.admin= '$user_id' ";
            }
            $count = Yii::app()->db->createCommand($sql)->queryScalar();
            $c = (int)$count;
            $data[] = array('name' => $row['pilihan'],'data' => array($c));
        endforeach;
          $this->render('export',array(
            'title' => $soal_model,
            'yTitle' => 'Jumlah',
            'xAxis' => $x,
            'data' => $data,
        ));
     }
     
     public function actionExportXLS2($id)
     {
         if(empty($_GET['id']))
         {
             throw new CHttpException(404);
         }
           $title = Soal::model()->findByPk($id);
        $yTitle = 'Jumlah';
        $x = array();
        $data = array();
        $data2 = array();
         $sql = "
             SELECT  jawaban_kompetensi.jawaban_a , jawaban_kompetensi.jawaban_b
             FROM jawaban_kompetensi
             LEFT JOIN soal ON soal.id = jawaban_kompetensi.soal 
             LEFT JOIN mahasiswa ON mahasiswa.id = jawaban_kompetensi.mahasiswa
             WHERE jawaban_kompetensi.soal = '$id'";
         $sql .= ";";
         if(isset(Yii::app()->session['query']))
         {
             $s = " WHERE jawaban_kompetensi.soal = '$id' ";
             $sql = Yii::app()->session['query'].$s.Yii::app()->session['query_end'];
         }
         $xTitle = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($xTitle as $row) :
            $data[] = $row['jawaban_a'];
            $data2[] = $row['jawaban_b'];
        endforeach;
        $this->render('export2',array(
            'title' => $title,
            'data' => $this->countValue($data),
            'data2' => $this->countValue($data2),
        ));
     }
}