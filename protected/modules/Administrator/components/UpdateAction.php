<?php

class UpdateAction extends MAction
{
    public $redirectTo = 'index';
    public $id;
    public $modelClass;
    public $formClass;
    public $submit = 'Click';
    public $form = 'form-id';
    
    public function run()
    {
        $this->check();
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $model = CActiveRecord::model($this->modelClass)->findByPk($this->id);
        if(!$model)
        {
            throw new CHttpException(404);
        }
        
        $form = new CForm($this->requestFormView().$controller->getId().$this->formClass,$model);
        $controller->performAJAXValidation($model,$this->form);
        if($form->submitted($this->submit))
        {
            $model->attributes = $_POST[$this->modelClass];
            $model->modified = $this->getCurrentTime();
            if($model->update())
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Data berhasil diubah');
                $controller->redirect($this->requestBaseUrl().ucfirst($controller->getId()).$this->redirectTo);
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Data gagal untuk diubah');
                $controller->refresh();
            }
        }
        $controller->render($this->_form,array('form' => $form,'model' => $model));
    }
    
    public function check()
    {
        if(empty($_GET['id']))
        {
            throw new CHttpException(404);
        }
        else
        {
            $this->id = $_GET['id'];
        }
    }
}