<?php

class ListDataAction extends MAction
{
    public $controller;
    public $modelClass;
    public $criteria;
    public $sort;
    public $view;
    public $pageSize = 30;
    
    public function run()
    {
        if($this->criteria == NULL)
        {
            $this->criteria = array();
        }
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        if(Yii::app()->user->role == 3) 
        {
            $user = Admin::model()->findByPk(Yii::app()->user->id);
            $this->criteria = array('condition' => 'admin= '.$user->id);
        }
        if(Yii::app()->user->role == 5) 
        {
            $user = Admin::model()->findByPk(Yii::app()->user->id);
            $this->criteria = array('condition' => 'prodi = '.$user->prodi);
        }
        
        $dataProvider = new CActiveDataProvider($this->modelClass,array(
            'criteria' => $this->criteria,
            'sort' => $this->sort,
            'pagination' => array(
                'pageSize' => $this->pageSize,
            ),
        ));
        $controller->render($this->view,array('dataProvider' => $dataProvider));
    }
}