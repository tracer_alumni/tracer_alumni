<?php

class ViewAction extends MAction
{
    public $modelClass;
    public $id;
    public $view;
    
    public function run()
    {
        $this->check();
        $this->getView();
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $model = CActiveRecord::model($this->modelClass)->findByPk($this->id);
        $controller->render($this->view,array('model' => $model));
    }
    
    public function check()
    {
        if(empty($_GET['id']))
        {
            throw new CHttpException(404);
        }
        else
        {
            $this->id = $_GET['id'];
        }
    }
    
    public function getView()
    {
        if($this->view == NULL)
        {
            $this->view = $this->_detail;
        }
    }
}