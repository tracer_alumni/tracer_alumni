<?php

class SearchAction extends MAction
{
    public $view;
    public $view_result;
    public $modelClass;
    public $pageSize = '20';
    public $controller;
    
    public function run()
    {
        $this->controller = $this->getController();
        $this->checkModel();
        $this->checkView();
        $this->checkResult();
        
        $model = new $this->modelClass('search');
        $model->unsetAttributes();
        if(isset($_POST[$this->modelClass]))
        {
            $model->attributes = $_POST[$this->modelClass];
            $this->controller->render($this->view_result,array('dataProvider' => $model->search()));
        }
        else
        {
            $this->controller->render($this->view,array('model' => $model));
        }
    }
    
    public function checkModel()
    {
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($this->controller->getId());
        }
    }
    
    public function checkView()
    {
        if($this->view == NULL)
        {
            $this->view = '/layouts/search_view';
        }
    }
    
    public function checkResult()
    {
        if($this->view_result == NULL)
        {
            throw new CHttpException(404);
        }
    }
}