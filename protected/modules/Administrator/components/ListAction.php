<?php

class ListAction extends MAction
{
    public $view;
    public $modelClass;
    public $criteria;
    public $sort;
    public $pageSize = 30;
    
    public function run()
    {
        $this->checkView();
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $this->checkCriteria();
        $dataProvider = new CActiveDataProvider($this->modelClass,array(
            'sort' => $this->sort,
            'criteria' => $this->criteria,
            'pagination' => array(
                'pageSize' =>$this->pageSize
            ),
        ));
        $controller->render($this->view,array('dataProvider' => $dataProvider));
    }
    
    public function checkCriteria()
    {
        if($this->criteria == NULL)
        {
            $this->criteria = new CDbCriteria();
        }
    }
    
    public function checkView()
    {
        if($this->view == NULL)
        {
            $this->view = $this->_view;
        }
    }
}