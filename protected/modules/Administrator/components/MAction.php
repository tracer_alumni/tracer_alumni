<?php

/**
 * This class is extends CAction 
 * The different is MAction has some variable like $_form to perform default form to form builder,
 * and a function to get a baseUrl , formview
 */

class MAction extends CAction
{
    /**
     * @var string default form to form builder
     */
    public $_form ='/layouts/form';
    
    /**
     * @property default view to render a data provider
     * @var string default list view to render a data provider
     */
    public $_view = '/layouts/listview';
    
    /**
     * 
     * @var string default detail view
     */
    public $_detail = '/layouts/view';
    
    public function requestBaseUrl()
    {
        return Yii::app()->request->baseUrl.'/Administrator/';
    }
    
    public function requestFormView()
    {
        return 'application.modules.Administrator.views.';
    }
    
    public function getCurrentTime()
    {
        $date = date('Y-m-d H:i:s').'.000000';
        return $date;
    }
    
    public function checkParam($param)
    {
    }
}