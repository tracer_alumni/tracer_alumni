<?php

class DeleteAction extends MAction
{
    public $redirectTo = 'index';
    public $id;
    public $modelClass;
    
    public function run()
    {
        $this->check();
        
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $model = CActiveRecord::model($this->modelClass)->findByPk($this->id);
        if(!$model)
        {
            throw new CHttpException(404);
        }
        if($model->delete())
        {
            Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Data berhasil dihapus');
        }
        else
        {
            Yii::app()->user->setFlash(BstHml::ALERT_COLOR_ERROR, 'Data gagal dihapus');
        }
        $controller->redirect($this->requestBaseUrl().ucfirst($controller->getId()).$this->redirectTo);
    }
    
    public function check()
    {
        if(empty($_GET['id']))
        {
            throw new CHttpException(404);
        }
        else
        {
            $this->id = $_GET['id'];
            echo $this->id;
        }
    }
}