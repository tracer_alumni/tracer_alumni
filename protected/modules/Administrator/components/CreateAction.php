<?php

class CreateAction extends MAction
{
    public $redirectTo = 'index';
    public $modelClass;
    public $formClass;
    public $submit = 'Click';
    public $form = 'form-id';
    
    public function run()
    {
        $controller = $this->getController();
        if($this->modelClass == NULL)
        {
            $this->modelClass = ucfirst($controller->getId());
        }
        $model = new $this->modelClass;
        $form = new CForm($this->requestFormView().$controller->getId().$this->formClass,$model);
        $this->getController()->performAJAXValidation($model,$this->form);
        if($form->submitted($this->submit))
        {
            $model->attributes = $_POST[$this->modelClass];
            $model->created = $this->getCurrentTime();
            $model->modified = $this->getCurrentTime();
            
            if($model->save())
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_SUCCESS, 'Data berhasil dimasukkan');
                $controller->redirect($this->requestBaseUrl().ucfirst($controller->getId()).$this->redirectTo);
            }
            else
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Data gagal untuk dimasukkan');
                $controller->refresh();
            }
        }
        $controller->render($this->_form,array('form' => $form,'model'=> $model));
    }
}