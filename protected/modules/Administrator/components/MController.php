<?php

class MController extends CController
{
    public $periods = array("detik","menit", "jam", "hari", "minggu", "bulan", "tahun", "dekade" );
    public $lengths = array("100","60","60","24","7","4.35","12","10");
    
    /**
     * @var string array the breadcrumb current page
     */
    public $breadcrumbs = array();
    
    /**
     * Default layout
     */
    public $layout = '/layouts/main';
    
    /**
     * Default form use in form builder
     */
    public $_form = '/layouts/form';
    
    public function performAjaxValidation($model,$form)
    {
        if(isset($_POST['ajax']) && $_POST['ajax'] === $form)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    /**
     * Base Url for Administrator
     * @return string
     */
    public function requestBaseUrl()
    {
        return Yii::app()->request->baseUrl.'/Administrator/';
    }
    
    public function checkParam($param)
    {
        if(empty($_GET[$param]))
        {
            throw new CHttpException(404);
        }
    }
    
    public function filters()
    {
        return array(
            /*array(
                'ext.compressor.ECompressorHtml',
            ),*/
        );
    }
     public function getCurrentTime()
    {
        $date = date('Y-m-d H:i:s').'.000000';
        return $date;
    }
}