<div class="row">
    <div class="col-lg-6">
        <?php $this->widget('bootstrap.widgets.BsAlert');?>
        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm',array('id' => 'form-id'));?>
        <div class="form-group">
            <label>Katasandi sebelumnya</label>
            <input type="password" maxlength="12" name="Admin[katasandi_lama]" id="Admin_katasandi_lama" class="form-control" placeholder="Masukkan katasandi sebelumnya"/>
        </div>
        <div class="form-group">
            <label>Katasandi baru</label>
            <input type="password" maxlength="12" name="Admin[katasandi_baru]" id="Admin_katasandi_baru" class="form-control" placeholder="Masukkan katasandi baru"/>
        </div>
        <div class="form-group">
            <label>Konfirmasi Katasandi baru</label>
            <input type="password" maxlength="12" name="Admin[konfirmasi_katasandi]" id="Admin_konfirmasi_katasandi" class="form-control" placeholder="Konfirmasi katasandi baru"/>
        </div>
        <div class="form-group" align="center">
            <input type="submit" value="Ubah Katasandi" class="btn btn-primary"/>
        </div>
        <?php $this->endWidget();?>
    </div>
</div>