<?php
$this->widget('bootstrap.widgets.BsAlert');
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_HOVER,
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'nama_lengkap',
            'header' => 'Nama Lengkap',
       ),
       array(
            'name' => 'nama_akun',
            'header' => 'Nama Akun',
       ),
       array(
           'name' => 'role',
           'value' => '$data->getRole($data->role)',
           'header' => 'Role',
       ),
        array(
            'name' => 'created',
            'value' => 'date("d-M-Y",strtotime($data->created))',
            'header' => 'Sejak',
        ),
       array(
           'class' => 'CButtonColumn',
        ),
   ),
));
?>