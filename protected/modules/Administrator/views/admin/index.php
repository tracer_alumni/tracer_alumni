<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nama_lengkap' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan nama lengkap',
            'maxlength' => 45,
            'value' => $this->model->nama_lengkap,
        ),
        'gelar' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan gelar',
            'maxlength' => 12,
            'value' => $this->model->gelar,
        ),
         'nama_akun' => array(
            'type' => 'text',
             'class' => 'form-control',
            'placeholder' => 'Masukkan nama akun',
            'maxlength' => 12,
            'value' => $this->model->nama_akun,
        ),
         'katasandi' => array(
            'type' => 'password',
             'class' => 'form-control',
            'placeholder' => 'Masukkan katasandi',
            'maxlength' => 12,
             'value' => ''
        ),
         'role' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
             'items' => Admin::model()->cboListRole(),
             'onblur' => 'dependent()',
        ),
         'prodi' => array(
            'type' => 'dropdownlist',
            'class' => 'form-control',
            'items' => CHtml::listData(Prodi::model()->findAll(array('order' => 'nama_prodi ASC')),'id','nama_prodi'),
            'prompt' => 'Pilih',
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Tambahkan' : 'Simpan',array(
                'name' => 'Click',
            )),
        ),
);
?>