<table class="table table-bordered detail-view" id="yw0">
    <tr class="even">
        <th>Nama lengkap</th>
        <td><?php echo $model->nama_lengkap.'.'.$model->gelar;?></td>
    </tr>
    <tr class="odd">
        <th>Nama akun</th> 
        <td><?php echo $model->nama_akun;?></td>
    </tr>
    <tr class="even">
        <th>Status</th> 
        <td><?php echo $model->status ? 'Aktif' : 'Nonaktif';?></td>
    </tr>
    <tr class="odd">
        <th>Role</th> 
        <td><?php 
        $role = $model->role;
        if($role == 1)
        {
            echo 'Super Administrator';
        }else if($role == 2)
        {
            echo 'Administrator';
        }else {
            echo 'Surveyor';
        }?></td>
    </tr>
</table>