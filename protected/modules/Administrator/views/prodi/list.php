<?php
$this->widget('bootstrap.widgets.BsAlert');
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'type' => BsHtml::GRID_TYPE_CONDENSED,
    'type' => BsHtml::GRID_TYPE_HOVER,
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'fakultas',
            'value' => '$data->jurusan0->fakultas0->nama_fakultas',
            'header' => 'Fakultas',
        ),
        array(
            'name' => 'jurusan',
            'value' => '$data->jurusan0->nama_jurusan',
            'header' => 'Jurusan',
       ),
       array(
            'name' => 'nama_prodi',
            'header' => 'Nama',
       ),
       array(
           'class' => 'CButtonColumn',
           'template' => '{update} {delete}'
       ),
    ),
));
?>