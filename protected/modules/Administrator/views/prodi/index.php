<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'jurusan' => array(
            'type' => 'dropdownlist',
            'class' => 'form-control',
            'items' => CHtml::listData(Jurusan::model()->findAll(array('order' => 'nama_jurusan ASC')),'id','nama_jurusan'),
        ),
        'nama_prodi' => array(
            'type' => 'text',
             'class' => 'form-control',
            'placeholder' => 'Masukkan nama prodi',
            'maxlength' => 100,
            'value' => $this->model->nama_prodi,
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Tambahkan' : 'Simpan',array(
                'name' => 'Click',
            )),
        ),
);
?>