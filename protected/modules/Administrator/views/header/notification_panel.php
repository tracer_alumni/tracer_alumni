<?php 
function RelativeTime( $timestamp,$periods,$lengths){
    if( !is_numeric( $timestamp ) ){
        $timestamp = strtotime( $timestamp );
        if( !is_numeric( $timestamp ) ){
            return "";
        }
    }
    $difference = time() - $timestamp;
    if ($difference > 0) { 
        $ending = "yang lalu";
    }else {
        $difference = -$difference;
        $ending = "akan datang";
    }
    for( $j=0; $difference>=$lengths[$j] and $j < 7; $j++ )
        $difference /= $lengths[$j];
    $difference = round($difference);
    if( $difference != 1 ){
        $periods[$j].= "";
    }
    $text = "$difference $periods[$j] $ending";
    return $text;
}
$i=0;
foreach($model as $row) : $i++;?>
<a href="<?php echo Yii::app()->request->baseUrl.'/Administrator/mahasiswa/view/id/'.$row['id'];?>" class="list-group-item">
    <i class="fa fa-user fa-fw"></i> <?php echo $row['nama'];?>
    <span class="pull-right text-muted small"><em><?php echo relativeTime($row['created'],$periods,$lengths);?></em></span>
</a>
<?php endforeach;?>