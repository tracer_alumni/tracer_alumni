<?php 
function RelativeTime( $timestamp,$periods,$lengths){
    if( !is_numeric( $timestamp ) ){
        $timestamp = strtotime( $timestamp );
        if( !is_numeric( $timestamp ) ){
            return "";
        }
    }
    $difference = time() - $timestamp;
    if ($difference > 0) { 
        $ending = "yang lalu";
    }else {
        $difference = -$difference;
        $ending = "akan datang";
    }
    for( $j=0; $difference>=$lengths[$j] and $j < 7; $j++ )
        $difference /= $lengths[$j];
        $difference = round($difference);
    if( $difference != 1 ){
        $periods[$j].= "";
    }
    $text = "$difference $periods[$j] $ending";
    return $text;
}
$i=0;
foreach($model as $row) : $i++;?>
<li class="clearfix">
    <a href="<?php echo Yii::app()->request->baseUrl.'/Administrator/mahasiswa/view/id/'.$row['id'];?>">
        <div>
            <?php echo $row['nama'];?>
            <small class="pull-right muted"><i class="fa fa-clock-o fa-fw"></i> <?php echo relativeTime($row['created'],$periods,$lengths);?></small>
        </div>
    </a>
</li>
<li class="divider"></li>
<?php endforeach;?>

<?php if($total > 0) : $now = date('Y-m-d');?>
<li>
    <a class="text-center" href="<?php echo Yii::app()->request->baseUrl."/Administrator/mahasiswa/index/";?>">
        <strong>Lihat semua (<?php echo $total;?>)</strong>
        <i class="fa fa-angle-right"></i>
    </a>
</li>
<?php else :?>
<li>
    <a class="text-center" href="#">
        <strong>Tidak ada pemberitahuan</strong>
    </a>
</li>
<?php endif; ?>
