<?php
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_CONDENSED,
    'type' => BsHtml::GRID_TYPE_HOVER,
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'class' => 'CLinkColumn',
            'labelExpression' => '$data->nim',
            'header' => 'NIM',
            'urlExpression' => ' array("mahasiswa/view/id/".Mahasiswa::model()->changeNIMToId($data->nim))',
       ),
        array(
            'name' => 'nama',
            'header' => 'Nama Alumni'
       ),
       array(
            'name' => 'saran',
            'header' => 'Saran',
       ),
        array(
            'name' => 'created',
            'header' => 'Tanggal',
            'value' => 'date("d-M-Y",strtotime($data->created))',
        )
    ),
));
?>