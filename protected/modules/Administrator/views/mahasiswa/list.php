<?php
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_CONDENSED,
    'type' => BsHtml::GRID_TYPE_HOVER,
   'dataProvider' => $dataProvider,
   'columns' => array(
        array(
            'name' => 'nim',
            'header' => 'NIM',
        ),
        array(
            'name' => 'nama',
            'header' => 'Nama',
        ),
       array(
           'name' => 'nama_prodi',
           'header' => 'Prodi',
       ),
       array(
           'name' => 'tahun_masuk',
           'header' => 'Tahun Masuk',
       ),
       array(
           'name' => 'admin',
           'value' => 'isset($data->admin) ? Admin::model()->getAdmin($data->admin) : "Mandiri"',
           'header' => 'Surveyor'
       ),
       array(
           'name' => 'created',
           'value' => 'date("d-M-Y",strtotime($data->created))',
           'header' => 'Data Masuk',
       ),
       array(
           'class' => 'CButtonColumn',
           'template' => '{reply} {view} {delete}',
           'buttons' => array(
               'reply' => array(
                   'label' => 'Answer',
                   'url' => 'CHtml::normalizeUrl(array("answer/id/"))',
                   'url' => 'CHtml::normalizeUrl(array("mahasiswa/answer/id/".rawurlencode($data->id)))',
                   'imageUrl' => Yii::app()->baseUrl.'/images/other/tag.png',
               ),
           ),
       ),
    ),
));
?>