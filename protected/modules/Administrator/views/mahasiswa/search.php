<div class="row">
    <div class="col-lg-6">
        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm',array('id' => 'form-id'));?>
        <div class="form-group">
            <label><?php echo $form->label($model,'nim');?></label>
            <?php echo $form->textField($model,'nim');?>
        </div>
        <div class="form-group">
            <label><?php echo $form->label($model,'nama');?></label>
            <?php echo $form->textField($model,'nama');?>
        </div>
        <div class="form-group">
            <label><?php echo $form->label($model,'tahun_masuk');?></label>
            <?php echo $form->dropDownList(Mahasiswa::model(),'tahun_masuk',CHtml::listData(Mahasiswa::model()->getYear(),'id','tahun'));?>
        </div>
        <div class="form-group">
            <label><?php echo $form->label($model,'tahun_lulus');?></label>
            <?php echo $form->dropDownList(Mahasiswa::model(),'tahun_lulus',CHtml::listData(Mahasiswa::model()->getYear(),'id','tahun'));?>
        </div>
        <div class='form-group'>
            <label>Data Masuk Dari Tanggal</label>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name'=>"Mahasiswa[from_date]",
                'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'form-control',
                    'placeHolder' => 'Data masuk dari tanggal',
                ),));?>
        </div>
         <div class='form-group'>
            <label>Data Masuk Sampai Tanggal</label>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name'=>"Mahasiswa[to_date]",
                'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'form-control',
                    'placeHolder' => 'Data masuk sampai tanggal',
                ),));?>
        </div>
        <div class="form-group" align="center">
            <?php echo BsHtml::submitButton('Cari',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>
        </div>
        <?php $this->endWidget();?>
    </div>
</div>