<?php
function getKeterangan($id)
{
    $ket = '';
    switch ($id) {
        case 5:
            return  'Sangat Bagus';
            break;
        case 4:
            return  'Baik';
            break;
        case 3:
            return 'Cukup';
            break;
        case 2:
            return 'Kurang';
            break;
        case 1:
            return 'Sangat Kurang';
            break;
    }
}?>
<table class="detail-view table custom-table">
    <thead>
        <th>Nomor</th>
        <th>Pertanyaan</th>
    </thead>
    <?php
    foreach($soal as $soals) :?>
    <tr>
        <td><?php echo $soals->nomor;?></td>
        <td><?php echo $soals->pertanyaan;?></td>
    </tr>
    <tr>
        <td id="no-border"></td>
        <td id="no-border">
            <?php foreach($soals->pilihans as $row) :
                foreach($row->jawabans as $j) :
                    if($j->mahasiswa == $mahasiswa_id)
                    {
                        echo "<p>".$j->pilihan0->pilihan.'   <strong>'.$j->keterangan.'</strong></p>';
                    }
                endforeach;
            endforeach;?>
        </td>
    </tr>
    <?php endforeach;?>
    
    <?php foreach($soal4 as $soals4) :?>
    <tr>
        <td><?php echo $soals4->nomor;?></td>
        <td><?php echo $soals4->pertanyaan;?></td>
    </tr>
    <tr>
        <td id="no-border"></td>
        <td id="no-border">
            <?php foreach($soals4->jawabanKompetensis as $row4) :
                if($row4->mahasiswa == $mahasiswa_id)
                {
                    echo "<p>Pada saat lulus,pada tingkat mana kompetensi dibawah ini anda kuasai : <b>".getKeterangan ($row4->jawaban_a)."</b></p>";
                    echo "<p>Pada saat lulus,bagaimana kontribusi perguruan tinggi dalam hal kompetensi dibawah ini : <b>".getKeterangan($row4->jawaban_b)."</b></p>";
                }?>
            <?php endforeach; ?>
        </td>
    </tr>
    <?php endforeach;?>
    <tr>
        <td>Saran</td>
        <td><?php echo $saran;?></td>
    </tr>
</table>