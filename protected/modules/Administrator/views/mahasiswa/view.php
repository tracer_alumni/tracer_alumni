<?php
$this->widget('bootstrap.widgets.BsDetailView', array(
    'type' => BsHtml::GRID_TYPE_BORDERED,
    'data'=>$model,
    'attributes'=>array(
        'nim',
        'nama',
        array(
            'name' => 'Fakultas',
            'value' => $model->prodi0->jurusan0->fakultas0->nama_fakultas,
        ),
        array(
            'name' => 'Jurusan',
            'value' => $model->prodi0->jurusan0->nama_jurusan,
        ),
        array(
            'name' => 'Prodi',
            'value' => $model->prodi0->nama_prodi,
        ),
        'tempat_lahir',
        'tanggal_lahir',
        array(
            'name' => 'Jenis Kelamin',
            'value' => $model->jenis == 'L' ? 'Laki laki' : 'Perempuan',
        ),
        'alamat',
        'kota',
        'telepon',
        'email',
        'tahun_masuk',
        'tahun_lulus',
        array(
            'name' => 'Lama Studi',
            'value' => $model->lama_studi.' tahun',
        ),
        'instansi',
        'telepon_instansi',
        'email_instansi',
        'tahun_masuk',
        array(
            'name' => 'Data Masuk',
             'value' => date("d-M-Y h:i:s",strtotime($model->created)),
        ),
    ),
));?>