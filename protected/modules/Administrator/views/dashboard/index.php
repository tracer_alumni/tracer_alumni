<div class="row">
    <div class="col-lg-4 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users  fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge" id="mahasiswa_total">0</div>
                        <div>Jumlah Alumni</div>
                    </div>
                </div>
            </div>
            <a href="<?php echo Yii::app()->request->baseUrl.'/Administrator/mahasiswa/index';?>">
                <div class="panel-footer">
                    <span class="pull-left">Lebih Rinci</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users  fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge" id="mahasiswa_today">0</div>
                        <div>Alumni Masuk Hari Ini</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Lebih Rinci</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-pencil  fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge" id="mahasiswa_admin">0</div>
                        <div>Alumni Masuk Hari Ini (Surveyor)</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">Lebih Rinci</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
<!--end of panel-->

<!--side-->
<div class="row">
    <div class="col-lg-8 col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-envelope fa-fw"></i> Data Masuk Oleh Surveyor
            </div>
            <div class="panel-body">
                <div class="list-group" id="surveyor_entry">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 pull-right col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Panel Pemberitahuan
            </div>
            <div class="panel-body">
                <div class="list-group" id="notification_panel">
                    <a href="#" class="list-group-item" id="notification_panel_loading"></a>
                </div>
                <a href="<?php echo Yii::app()->request->baseUrl.'/Administrator/mahasiswa/index';?>" class="btn btn-default btn-block btn-primary">Lihat Semua</a>
            </div>
        </div>
    </div>
</div>
<!--end of side-->