<div class="row">
    <div class="col-lg-12">
        <?php echo CHtml::link('Export',array('soal/exportXLS2/id/'.$_GET['id']));?> | 
        <a href="#" id="toggle-sharing">Saring</a>
        <div class="filter-form" style="display:none">
            <div id="loading-share" class="color-white">L</div>
            <div class="share"></div>
        </div>
        <?php 
        $id = $_GET['id'];
        echo  isset(Yii::app()->session['filter']) ? CHtml::link('| Matikan penyaringan',array('/Administrator/soal/filterOff/id/'.$id)) : '';?>
    </div>
    <br/>
    <div class="col-lg-6">
        <?php
$this->widget('ext.highcharts.HighchartsWidget',array(
    'options' => array(
        'exporting' => array('enabled' => true),    
        'scripts' => array(
            'modules/exporting.js   '
        ),
        'chart' => array(
            'defaultSeriesType' => 'column',
        ),
        'credits' => array(
            array('enabled' => 'false'),
        ),
        'title' => array('text' => $title),
        'xAxis' => array(
            'categories' => $xAxis,
        ),
        'yAxis' => array(
            'title' => array('text' => $yTitle),
        ),
        'series' => $data,
    ),
));
?>
        <p class="text-center">Tingkat Kompetensi yang dikuasai</p>
    </div>
    <div class="col-lg-6">
        <?php
$this->widget('ext.highcharts.HighchartsWidget',array(
    'options' => array(
        'exporting' => array('enabled' => true),    
        'scripts' => array(
            'modules/exporting.js   '
        ),
        'chart' => array(
            'defaultSeriesType' => 'column',
        ),
        'credits' => array(
            array('enabled' => 'false'),
        ),
        'title' => array('text' => $title),
        'xAxis' => array(
            'categories' => $xAxis,
        ),
        'yAxis' => array(
            'title' => array('text' => $yTitle),
        ),
        'series' => $data2,
    ),
));
?>
        <p class="text-center">Kontribusi Perguruan Tinggi</p>
    </div>
</div>