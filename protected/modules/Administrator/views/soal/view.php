<?php 
$this->widget('bootstrap.widgets.BsAlert');
$i=0;?>
<table class="detail-view table table-bordered table-condensed" id="yw0">
    <tr class="odd">
        <th>Nomor</th>
        <td><?php echo $model->nomor;?></td>
    </tr>
    <tr class="even">
        <th>Pertanyaan</th>
        <td><?php echo $model->pertanyaan;?></td>
    </tr>
    <?php foreach($model->pilihans as $pilihan) :
        $i++;?>
    <tr class="even">
        <th><?php echo 'Pilihan '.$i;?></th>
        <td>
            <?php echo $pilihan->pilihan;?>
            <?php echo CHtml::link('Delete',array('/Administrator/soal/deleteChoice/id/'.$pilihan->id.'/mid/'.$model->id));?>
            <?php echo CHtml::link('Edit',array('/Administrator/soal/editChoice/id/'.$pilihan->id.'/mid/'.$model->id));?>
        </td>
    </tr>
    <?php endforeach;?>
</table>

<?php 
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'id'=>'form-id',
    'enableAjaxValidation'=>true,
    'enableClientValidation' => false));
        echo $form->textField($new_answer,'pilihan',array('placeHolder' => 'Masukkan pilihan baru disini'));
        echo $form->error($new_answer,'pilihan',array('class' => 'errorMessage'));
        echo '<br/>';
        echo BsHtml::submitButton('Buat',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
$this->endWidget();?>