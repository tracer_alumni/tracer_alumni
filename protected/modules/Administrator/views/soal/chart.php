<div class="row">
    <div class="col-lg-12">
        <?php echo CHtml::link('Export',array('soal/exportXLS/id/'.$_GET['id']));?> | 
        <a href="#" id="toggle-sharing">Saring</a>
        <div class="filter-form" style="display:none">
            <div id="loading-share" class="color-white">L</div>
            <div class="share"></div>
        </div>
        <?php 
        $id = $_GET['id'];
        echo  isset(Yii::app()->session['filter']) ? CHtml::link('| Matikan penyaringan',array('/Administrator/soal/filterOff/id/'.$id)) : '';?>
        <?php
$this->widget('ext.highcharts.HighchartsWidget',array(
    'options' => array(
        'exporting' => array('enabled' => true),    
        'scripts' => array(
            'modules/exporting.js   '
        ),
        'chart' => array(
            'defaultSeriesType' => 'column',
        ),
        'credits' => array(
            array('enabled' => 'false'),
        ),
        'title' => array('text' => $title),
        'xAxis' => array(
            'categories' => $xAxis,
        ),
        'yAxis' => array(
            'title' => array('text' => $yTitle),
        ),
        'series' => $data,
    ),
));
?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
       <?php echo BsHtml::button('Tampilkan data lainnya',array('color' =>  BsHtml::BUTTON_COLOR_PRIMARY,'id' => 'display-other-colum-button'));?>
        <div id="loading-other-column" class="color-white">L</div>
        <div id="other_column"></div>
    </div>
</div>
<br/>
<?php
$url = Yii::app()->request->baseUrl.'/Administrator/soal/renderOther/id/'.$_GET['id'];
Yii::app()->clientScript->registerScript('getData', "
$('#display-other-colum-button').click(function(){
  $('#loading-other-column').addClass('loading-prog');
  $.ajax({
            url : '$url',
            cache : false,
            success : function(data){
                $('#other_column').html(data);
                $('#loading-other-column').removeClass('loading-prog');
            },
        });
});
");
?>