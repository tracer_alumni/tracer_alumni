<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'form-id',
        'enableAjaxValidation'=>true,
        'enableClientValidation' => false)
        ); ?>
    <div class="form-group  ">
        <?php echo $form->labelEx($model,'pilihan'); ?>
        <?php echo $form->textField($model,'pilihan'); ?>
        <?php echo $form->error($model,'pilihan'); ?>
    </div>
    <?php echo BsHtml::submitButton($model->isNewRecord ? 'Buat' : 'Simpan',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>
    <?php $this->endWidget();?>