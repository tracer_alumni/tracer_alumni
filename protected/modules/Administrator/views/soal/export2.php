<?php
$filename = $title->pertanyaan.'.xls';
header("Content-type: application/ms-excel");
header("Content-Disposition: attachment; filename=$filename");
?>
<table class="table table-bordered">
    <thead>
    <tr>
        <td colspan="2"><p class="text-center"><?php echo $title->pertanyaan;?></p></td>
    </tr>
    <tr>
        <td colspan="2"><?php echo Yii::app()->session['filter_display'];?></td>
    </tr>
    </thead>
    <?php foreach($data as $row) :?>
    <tr>
        <td><?php echo JawabanKompetensi::model()->getStatus($row['name']);?></td>
        <td><?php echo $row['data'][0];?> Pilihan</td>
    </tr>
    <?php endforeach;?>
    <tfoot>
        <td colspan="2"><p class="text-right">Tanggal : <?php echo date('Y-m-d h:i:s');?></p></td>
    </tfoot>
</table>
<?php 
exit();
?>