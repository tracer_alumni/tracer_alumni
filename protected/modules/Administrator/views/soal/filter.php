<?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm',array('id' => 'form-id'));?>
<div class="form-group">
    <?php if(Yii::app()->user->role <= 4) :?>
    <label>Pilih</label>
    <select class="form-control" name="Mahasiswa[pilih]" id="Mahasiswa_pilih" onblur="dependent()">
        <?php echo Soal::model()->cboDropDown();?>

    </select>
</div>
<div class="form-group">
    <label>Pilih</label>
    <select class="form-control" name="Mahasiswa[prodi]" id="Mahasiswa_prodi">
    </select>
</div>
<?php endif;?>
<div class="form-group">
    <label>Tahun Masuk</label>
    <?php echo $form->dropDownList(Mahasiswa::model(),'tahun_masuk',CHtml::listData(Mahasiswa::model()->getYear(),'id','tahun'));?>
</div>
<div class="form-group">
    <label>Tahun Lulus</label>
    <?php echo $form->dropDownList(Mahasiswa::model(),'tahun_lulus',CHtml::listData(Mahasiswa::model()->getYear(),'id','tahun'));?>
</div>
<div class="form-group">
    <label>Jenis Kelamin</label>
    <select name="Mahasiswa[jenis]" class="form-control">
        <option value="">Semua</option>
        <option value="L">Laki Laki</option>
        <option value="P">Perempuan</option>
    </select>
    <input type="hidden" value="<?php echo Yii::app()->session['soal_id'];?>" name="Soal[id]"/>
</div>
<div class="form-group" align="center">
    <?php echo BsHtml::submitButton('Saring',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>
</div>
<hr/>
<?php $this->endWidget();?>