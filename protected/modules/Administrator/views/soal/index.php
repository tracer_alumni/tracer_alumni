<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nomor' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan nomor soal',
            'maxlength' => 5,
            'value' => $this->model->nomor,
        ),
        'pertanyaan' => array(
            'type' => 'textarea',
            'class' => 'form-control',
            'placeholder' => 'Masukkan pertanyaan',
            'maxlength' => 200,
            'value' => $this->model->pertanyaan,
        ),
        'tipe' => array(
            'type' => 'dropdownlist',
            'class' => 'form-control',
            'items' => array('D'=> 'Hanya memiliki 1 jawaban','C' => 'Dapat memiliki lebih dari 1 jawaban'),
        ),
       'kolom' => array(
            'type' => 'dropdownlist',
           'class' => 'form-control',
            'items' => array('T'=> 'Text','I' => 'Angka','N' => 'Tidak'),
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Tambahkan' : 'Simpan',array(
                'name' => 'Click',
            )),
        ),
);
?>