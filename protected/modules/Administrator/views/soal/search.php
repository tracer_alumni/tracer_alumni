<div class="row">
    <div class="col-lg-6">
        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm',array('id' => 'form-id'));?>
        <div class="form-group">
            <label><?php echo $form->label($model,'nomor');?></label>
            <?php echo $form->textField($model,'nomor');?>
        </div>
        <div class="form-group">
            <label><?php echo $form->label($model,'pertanyaan');?></label>
            <?php echo $form->textField($model,'pertanyaan');?>
        </div>
        <div class="form-group" align="center">
            <?php echo BsHtml::submitButton('Cari',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>
        </div>
        <?php $this->endWidget();?>
    </div>
</div>