<?php
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_CONDENSED,
    'type' => BsHtml::GRID_TYPE_HOVER,
   'dataProvider' => $dataProvider,
   'columns' => array(
        array(
            'name' => 'nomor',
            'header' => 'Nomor',
        ),
        array(
            'name' => 'pertanyaan',
            'header' => 'Pertanyaan',
        ),
       array(
           'class' => 'CButtonColumn',
           'template' => '{reply}{view}{update}{delete}',
           'buttons' => array(
               'reply' => array(
                   'label' => 'Chart',
                   'url' => 'CHtml::normalizeUrl(array("chart/id/"))',
                   'url' => 'CHtml::normalizeUrl(array("soal/chart/id/".rawurlencode($data->id)))',
                   'imageUrl' => Yii::app()->baseUrl.'/images/other/tag.png',
               ),
           ),
       ),
    ),
));
?>