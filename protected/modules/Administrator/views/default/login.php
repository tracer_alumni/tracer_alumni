<div class="form">
    <?php $this->widget('bootstrap.widgets.BsAlert');?>
    <?php 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'admin-login-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'=>true,
        'enableClientValidation' => false)); ?>
    
    <p class="title">Masuk sebagai admin / surveyor</p>
    <table class="table custom-table">
        <tr>
            <td id="no-border">Nama Akun</td>
            <td id="no-border">:</td>
            <td id="no-border"><input type="text" name="LoginForm[nama_akun]" id="LoginForm_nama_akun" maxlength="12" class="form-control"/></td>
            <td id="no-border"><?php echo $form->error($model,'nama_akun');?></td>
        </tr>
        <tr>
            <td id="no-border">Katasandi</td>
            <td id="no-border">:</td>
            <td id="no-border"><input type="password" name="LoginForm[katasandi]" id="LoginForm_katasandi" maxlength="12" class="form-control"/></td>
            <td id="no-border"><?php echo $form->error($model,'katasandi');?></td>
        </tr>
        <tr>
            <td colspan="3" id="no-border"><?php echo BsHtml::submitButton('Masuk',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?></td>
        </tr>
    </table>
    <?php $this->endWidget();?>
</div>