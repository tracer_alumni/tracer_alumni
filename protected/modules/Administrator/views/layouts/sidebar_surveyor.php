<?php
$this->widget('zii.widgets.CMenu',array(
    'htmlOptions' => array('class' =>'nav','id'=>'side-menu'),
    'submenuHtmlOptions'=>array('class'=>'nav nav-second-level'),
    'activeCssClass'=>'active',
//'itemCssClass'=>'item-test',
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>'<i class="fa fa-users fa-fw"></i> Masukkan Data Alumni', 'url'=>array('/site/login')),
        array('label'=>'<i class="fa fa-users fa-fw"></i> Daftar Alumni', 'url'=>array('mahasiswa/')),
      ),
)); ?>