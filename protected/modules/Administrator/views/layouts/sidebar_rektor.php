<?php
$this->widget('zii.widgets.CMenu',array(
    'htmlOptions' => array('class' =>'nav','id'=>'side-menu'),
    'submenuHtmlOptions'=>array('class'=>'nav nav-second-level'),
    'activeCssClass'=>'active',
//'itemCssClass'=>'item-test',
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>'<i class="fa fa-users fa-fw"></i> Daftar Alumni', 'url'=>array('mahasiswa/')),
        array('label'=>'<i class="fa fa-question-circle fa-fw"></i> Kusioner<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Daftar Kusioner','url'=>array('soal/index')),
                array('label'=>'Daftar Saran','url'=>array('saran/index')),
          )),
      ),
)); ?>