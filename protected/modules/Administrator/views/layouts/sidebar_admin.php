<?php
$this->widget('zii.widgets.CMenu',array(
    'htmlOptions' => array('class' =>'nav','id'=>'side-menu'),
    'submenuHtmlOptions'=>array('class'=>'nav nav-second-level'),
    'activeCssClass'=>'active',
//'itemCssClass'=>'item-test',
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>'<i class="fa fa-dashboard fa-fw"></i> Dashboard', 'url'=>array('dashboard/')),
        array('label'=>'<i class="fa fa-users fa-fw"></i> Masukkan Data Alumni', 'url'=>array('/site/login')),
        array('label'=>'<i class="fa fa-users fa-fw"></i> Daftar Alumni', 'url'=>array('mahasiswa/')),
        array('label'=>'<i class="fa fa-question-circle fa-fw"></i> Kusioner<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Tambah Kusioner','url'=>array('soal/create')),
                array('label'=>'Daftar Kusioner','url'=>array('soal/index')),
                array('label'=>'Daftar Saran','url'=>array('saran/index')),
          )),
        array('label'=>'<i class="fa fa-paper-plane fa-fw"></i> Fakultas<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Tambah Fakultas','url'=>array('fakultas/create')),
                array('label'=>'Daftar Fakultas','url'=>array('fakultas/index')),
          )),
        array('label'=>'<i class="fa fa-paper-plane fa-fw"></i> Jurusan<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Tambah Jurusan','url'=>array('jurusan/create')),
                array('label'=>'Daftar Jurusan','url'=>array('jurusan/index')),
          )),
        array('label'=>'<i class="fa fa-paper-plane fa-fw"></i> Prodi<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Tambah Prodi','url'=>array('prodi/create')),
                array('label'=>'Daftar Prodi','url'=>array('prodi/index')),
          )),
        array('label'=>'<i class="fa fa-user fa-fw"></i> Pengguna<span class="fa arrow"></span>', 'url'=>'#',
            'items'=>array(
                array('label'=>'Tambah Pengguna','url'=>array('admin/create')),
                array('label'=>'Daftar Pengguna','url'=>array('admin/index')),
          )),
      ),
)); ?>