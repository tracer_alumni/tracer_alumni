<div class="row">
    <div class="col-lg-6">
        <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm',array('id' => 'form-id'));?>
            <?php foreach($model ->attributeLabels() as $row=>$value) : 
                if($row == "id" or $row == 'created' or $row == "modified") {} else {?>
                <div class="form-group">
                    <label><?php echo $value;?></label>
                    <?php echo $form->textField($model, strtolower($row),array('class' => 'form-control'));?>
                </div>
        <?php }
        endforeach;?>
        <div align="center">
            <?php echo BsHtml::submitButton('Cari',array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>
        </div>
        <?php $this->endWidget();?>
    </div>
</div>