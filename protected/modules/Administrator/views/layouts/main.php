<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <?php Yii::app()->bootstrap->register();?>
        <?php Yii::app()->clientScript->registerCoreScript("jquery");?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/script.js',  CClientScript::POS_END);?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/sb-admin-2.js',  CClientScript::POS_END);?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/metisMenu.min.js',  CClientScript::POS_END);?>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/metisMenu.min.css';?>"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/font-awesome/css/font-awesome.min.css';?>" type="text/css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/style.css';?>"/>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
         <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/sb-admin-2.css';?>"/>
        
    </head>
    
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php echo CHtml::link(Yii::app()->name, array('/Administrator'),array('class' => 'navbar-brand'));?>
                </div>
                
                <?php if(!Yii::app()->user->isGuest) : ?>
                <ul class="nav navbar-top-links navbar-right">
                    <?php if(Yii::app()->user->role <= 2) :?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="notification" title="Notifikasi">
                            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li id="loading-user">L</li>
                        </ul>
                        <?php endif;?>
                    </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Pengguna">
                                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><?php echo CHtml::link('<i class="fa fa-user fa-fw"></i> '.Yii::app()->user->name,array('/Administrator/dashboard/index'));?></li>
                                <li><?php echo CHtml::link('<i class="fa fa-shield fa-fw"></i> Ganti katasandi',array('/Administrator/admin/changePassword'));?></li>
                                <li class="divider"></li>
                                <li><?php echo CHtml::link('<i class="fa fa-sign-out fa-fw"></i> Keluar',array('/Administrator/default/logout'));?></li>
                            </ul>
                        </li>
                 </ul>
                
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <?php
                            $role = Yii::app()->user->role;
                            if(Yii::app()->user->isGuest)
                            {
                                $name = "sidebar_empty";
                            }
                            if($role <= 2)
                            {
                                $name = "sidebar_admin";
                            }
                            else if($role == 3)
                            {
                                $name = "sidebar_surveyor";
                            }
                            else if($role >= 4)
                            {
                                $name = "sidebar_rektor";
                            }
                            include $name.'.php';
                            ?>
                        </ul>
                    </div>
                </div>
                <?php endif;?>
            </nav>
            
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                            <h1 class="page-header">

                                <?php if (strtolower($this->id) == 'default') {
                                    echo "Halaman Admin/Surveyor";
                                } elseif (strtolower($this->id) == 'mahasiswa') {
                                    echo "Alumni";
                                }else{
                                    echo ucfirst($this->id);
                                }
                                ?>
                                <span class="pull-right search-title">
                                    <?php if(strtolower($this->id) == 'soal' or strtolower($this->id) == 'mahasiswa') { echo CHtml::link('Cari',array('search'),array('id' =>'toggle-modal')); }?>
                                </span>
                            </h1>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $content;?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <hr style="border-bottom: #fff solid 1px"/>
                        <?php echo 'Dibuat Oleh '.CHtml::link('IT Konsultan','http://itkonsultan.co.id/');?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
