<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('bootstrap.widgets.BsAlert');?>
    </div>
    <div class="col-lg-6">
        <?php echo $form->renderBegin();?>
            <?php foreach($form->getElements() as $element) :?>
                <div class="form-group">
                    <label><?php echo $element->getLabel();?></label>
                    <?php echo $element->renderInput();?>
                    <?php echo $element->renderError();?>
                </div>
                <?php endforeach;?>
            <div align="center">
                <?php echo $form->renderButtons();?>
            </div>
        <?php echo $form->renderEnd();?>
    </div>
</div>