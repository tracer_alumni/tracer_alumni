<?php
$this->breadcrumbs=array(
    $this->module->id => Yii::app()->request->baseUrl.'/'.$this->module->id,
    ucfirst($this->id),
);
$this->widget('bootstrap.widgets.BsGridView', array(
    'type' => BsHtml::GRID_TYPE_CONDENSED,
    'type' => BsHtml::GRID_TYPE_HOVER,
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'fakultas',
            'value' => '$data->fakultas0->nama_fakultas',
            'header' => 'Fakultas',
       ),
       array(
            'name' => 'nama_jurusan',
            'header' => 'Nama',
       ),
       array(
           'class' => 'CButtonColumn',
           'template' => '{update} {delete}'
       ),
    ),
));
?>