<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'fakultas' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
            'items' => CHtml::listData(Fakultas::model()->findAll(array('order' => 'nama_fakultas ASC')),'id','nama_fakultas'),
        ),
        'nama_jurusan' => array(
            'type' => 'text',
             'class' => 'form-control',
            'placeholder' => 'Masukkan nama jurusan',
            'maxlength' => 100,
            'value' => $this->model->nama_jurusan,
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Tambahkan' : 'Simpan',array(
                'name' => 'Click',
            )),
        ),
);
?>