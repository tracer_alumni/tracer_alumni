<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nama_fakultas' => array(
            'type' => 'text',
            'placeholder' => 'Masukkan nama fakultas',
            'maxlength' => 100,
            'value' => $this->model->nama_fakultas,
            'class' => 'form-control',
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Tambahkan' : 'Simpan',array(
                'name' => 'Click',
            )),
        ),
);
?>