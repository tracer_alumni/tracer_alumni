<?php

class SiteController extends Controller
{
    public $mid;
    public $array = array(
        'tracer1','tracer02','tracer2','tracer3','tracer4','tracer5'
    );
    
     public function actions() {
        return array(
            'tracer' => array(
                'class' => 'CreateAnswerAction',
                'modelClass' => 'mahasiswa',
                'criteria' =>array('condition' => 'nomor <= 1.01','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer02',
                //6 => Ya
                'cond1' => '6',
                'resp1' => 'site/tracer12',
                //54 => Tidak
                'cond2' => '54',
                'resp2' => 'site/tracer02',
                'prog' => '2',
                'session_name' => 'tracer1',
            ),
            'tracer02' => array(
                'class' => 'CreateAnswerAction',
                'modelClass' => 'mahasiswa',
                'criteria' =>array('condition' => 'nomor >= 1.02 AND nomor <=1.07','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer2',
                //53 => Ya
                'cond1' => '53',
                'resp1' => 'site/tracer11',
                //54 => Tidak
                'cond2' => '54',
                'resp2' => 'site/tracer12',
                'prog' => '2',
                'session_name' => 'tracer02',
            ),
            //Tracer11 merujuk kepada no 1.11
            'tracer11' => array(
                'class' => 'CreateAnswerAction',
                'modelClass' => 'mahasiswa',
                'criteria' =>array('condition' => 'nomor >= 1.11 AND nomor < 2 ','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer2',
                'prog' => '2',
                'session_name' => 'tracer2',
            ),
            //Tracer12 merujuk kepada no 1.8
             'tracer12' => array(
                'class' => 'CreateAnswerAction',
                'modelClass' => 'mahasiswa',
                'criteria' =>array('condition' => 'nomor >= 1.09 AND nomor <= 1.10','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer2',
                'prog' => '2',
                 'session_name' => 'tracer3',
            ),
            'tracer2' => array(
                'class' => 'CreateAnswerAction',
                'criteria' =>array('condition' => 'nomor >= 2 and nomor < 3','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer3',
                'prog' => '3',
                'session_name' => 'tracer4',
            ),
            'tracer3' => array(
                'class' => 'CreateAnswerAction',
                'criteria' =>array('condition' => 'nomor >= 3 and nomor < 4','order' => 'nomor ASC'),
                'view' => '//site/tracer',
                'redirectTo' => 'tracer4',
                'prog' => '4',
                'session_name' => 'tracer5',
            ),
        );
     }
    
    public function actionIndex()
    {
        unset(Yii::app()->session['progress']);
        Yii::app()->session['progress'] = '1';
         if(!Yii::app()->session['mahasiswa_validasi'])
         {
             Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR, 'Silahkan login terlebih dahulu');
             $this->redirect(array('/site/login'));
         }
        $model = new Mahasiswa;
        $form = $form = new CForm('application.views.site.index',$model);
        $this->performAjaxValidation($model,'form-id');
        if($form->submitted('Click') && $model->validate(array('tempat_lahir','alamat','kota','telepon','email','tahun_masuk','instansi','telepon_instansi','email_instansi')))
        {
            
            $model->nim = Yii::app()->session['mahasiswa_nim'];
            $model->nama = Yii::app()->session['mahasiswa_nama'];
            $model->tanggal_lahir = Yii::app()->session['mahasiswa_tgl_lhr'];
            $model->tahun_lulus = Yii::app()->session['mahasiswa_thn_lulus'];
            $model->prodi = Yii::app()->session['mahasiswa_prodi'];
            $model->lama_studi = $model->tahun_lulus  - $_POST['Mahasiswa']['tahun_masuk'];
            $model->attributes = $_POST['Mahasiswa'];
            if(!Yii::app()->user->isGuest)
            {
                $model->admin = Yii::app()->user->id;
            }
            $model->created = $this->getDateTime();
            $model->modified = $this->getDateTime();
            if(Mahasiswa::model()->findByAttributes(array('nim' => Yii::app()->session['mahasiswa_id'])))
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Anda sudah pernah mengisi data ini');
            }
            else
            {
                Yii::app()->session['data_mahasiswa'] = $model;
                $this->redirect(array('tracer'));
                /*
             * 
                if($model->save())
                {
                    unset(Yii::app()->session['mahasiswa_id']);
                    Yii::app()->session['mahasiswa_id'] = $model->id;
                    $this->redirect(array('tracer'));
                }
                else
                {
                    Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Data gagal dimasukkan');
                }*/
            }
        }
        $this->render('//layouts/form',array('form' => $form,'model' => $model));
    }
    
    public function actiontracer4()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/validation.js',  CClientScript::POS_END);
        Yii::app()->session['progress'] = '5';
         if(!isset(Yii::app()->session['data_mahasiswa']))
        {
             $this->redirect(array('index'));
        }
        $model = Soal::model()->findAll(array('condition' => 'nomor >= 4 and nomor< 5'));
        if(isset($_POST['Jawaban']))
        {
            $total = $_POST['Jawaban']['total'];
            $data = array();
            for($i=1;$i<=$total;$i++)
            {
                $soal = $_POST['Jawaban'.$i]['soal'];
                $jawaban_a = $_POST['Jawaban'.$i]['jawaban_a'];
                $data[] = array('soal' => $soal,'jawaban_a' => $jawaban_a);
            }
            Yii::app()->session['temp_answer_a'] = $data;
            $this->redirect(array('tracer5'));
        }
        $this->render('tracer4',array(
            'model' => $model,
            'judul' => 'A.Pada saat lulus,pada tingkat mana kompetensi dibawah ini anda kuasai',
            'keterangan' => 'Lanjut'
        ));
    }
    
    public function actiontracer5()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/validation.js',  CClientScript::POS_END);
        Yii::app()->session['progress'] = '6';
        if(!isset(Yii::app()->session['temp_answer_a']))
        {
            $this->redirect(array('tracer4'));
        }
        $model = Soal::model()->findAll(array('condition' => 'nomor >= 4 and nomor< 5'));
        if(isset($_POST['Jawaban']))
        {
            $this->mid = $this->SaveAnswer();
            if($this->mid >= 1)
            {
                $total = $_POST['Jawaban']['total'];
                $jawaban_a = Yii::app()->session['temp_answer_a'];
                for($i=1;$i<=$total;$i++) :
                    $soal = $_POST['Jawaban'.$i]['soal'];
                    /**
                     * Check is soal from tracer 5 same with tracer 4
                     * if same system will pass the A answer into soal model ->jawaban_a
                     * use $i-1 cause array is created at 0 index
                     */
                    if($soal == $jawaban_a[$i-1]['soal'])
                    {
                    $jawaban_b = $_POST['Jawaban'.$i]['jawaban_a'];
                    $jawaban_model = new JawabanKompetensi;
                    $jawaban_model->mahasiswa = $this->mid;
                    $jawaban_model->soal  = $soal;
                    $jawaban_model->jawaban_a = $jawaban_a[$i-1]['jawaban_a'];
                    $jawaban_model->jawaban_b = $jawaban_b;
                    $jawaban_model->save();

                    }
                endfor;
                $saran = new Saran;
                $saran->mahasiswa = $this->mid;
                $saran->saran = $_POST['Saran']['saran'];
                $saran->created = $this->getDateTime();
                $saran->save();
                unset(Yii::app()->session['mahasiswa_validasi']);
                unset(Yii::app()->session['mahasiswa_nama']);
                unset(Yii::app()->session['mahasiswa_tgl_lhr']);
                unset(Yii::app()->session['mahasiswa_thn_lulus']);
                unset(Yii::app()->session['temp_answer_a']);
                if(!Yii::app()->user->isGuest)
                {
                    $this->redirect(array('/Administrator/mahasiswa/index'));

                }
                unset(Yii::app()->session['progress']);
                $this->redirect(array('success'));
                $this->redirect(array('sukses'));
        }
        }
        $this->render('tracer4',array(
            'model' => $model,
            'judul' => 'B.Pada saat lulus,bagaimana kontribusi perguruan tinggi dalam hal kompetensi dibawah in',
            'keterangan' => 'Selesai'
        ));
    }
    
    public function actionLogin()
    {
        unset(Yii::app()->session['progress']);
        unset(Yii::app()->session['data_mahasiswa']);
        $model = new Mahasiswa;
        $form = new CForm('application.views.site.login',$model);
        if($form->submitted('Click'))
        {
            $is_avaiable = Mahasiswa::model()->findByAttributes(array('nim' => $_POST['Mahasiswa']['nim']));
            if($is_avaiable != NULL)
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Anda sudah pernah memasukkan data ini');
                $this->refresh();
            }
            $nim = strtoupper($_POST['Mahasiswa']['nim']);
            $nama = $_POST['Mahasiswa']['nama'];
            $tgl_lhr = $_POST['Mahasiswa']['tanggal_lahir'];
            $date = explode("-", $tgl_lhr);
            $thn_lulus = $_POST['Mahasiswa']['tahun_lulus'];
            $prodi = Prodi::model()->findByAttributes(array('nama_prodi' => $_POST['Mahasiswa']['prodi']))->id;
            /**
             * Web service untan
             * masih disable web servicenya
             */
            if($prodi == NULL || $thn_lulus == NULL)
            {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'Harap diisi dengan lengkap');
                $this->refresh();
            }
            //$response = file_get_contents('http://service.untan.ac.id:7777/datasnap/rest/tservermethods1/login/'.$nim.'/'.$date[0].$date[1].$date[2]);
            //Matikan kode dibawah ini jika ingin menggunakan web service
            $response= '{"result":["Login Sukses"]}';
            //Tidak didecode lagi karena akan memakan proses lebih lama sehingga langsung dipakai di if
            if($response == '{"result":["Login Sukses"]}')
            {
                unset(Yii::app()->session['mahasiswa_validasi']);
                Yii::app()->session['mahasiswa_validasi'] = true;
                Yii::app()->session['mahasiswa_nim'] = $nim;
                Yii::app()->session['mahasiswa_nama'] = $nama;
                Yii::app()->session['mahasiswa_tgl_lhr'] = $tgl_lhr;
                Yii::app()->session['mahasiswa_thn_lulus'] = $thn_lulus;
                Yii::app()->session['mahasiswa_prodi'] = $prodi;
                $this->redirect(array('/site/index'));
            }
            else {
                Yii::app()->user->setFlash(BsHtml::ALERT_COLOR_ERROR,'NIM atau Tanggal Lahir salah');
                $this->refresh();
            }
        }
        
        $this->render('/layouts/form_login',array('form' => $form,'model' => $model));
    }
    
    public function actionSuccess()
    {
        unset(Yii::app()->session['progress']);
        if(!isset(Yii::app()->session['mahasiswa_nim']))
        {
            $this->redirect(array('login'));
        }
        $id = Mahasiswa::model()->findByAttributes(array('nim' => Yii::app()->session['mahasiswa_nim']))->id;
        unset(Yii::app()->session['mahasiswa_nim']);
        $soal = Soal::model()->findAll(array('order' => 'nomor ASC','condition' => 'nomor < 4'));
        $soal4 = Soal::model()->findAll(array('order' => 'nomor ASC','condition' => 'nomor >= 4'));
        $saran = Saran::model()->findByAttributes(array('mahasiswa' => $id));
        $jawaban = Jawaban::model()->findAllByAttributes(array('mahasiswa' => $id));
        $this->render('success',array(
            'soal' => $soal,
            'soal4' => $soal4,
            'saran' => $saran,
            'jawaban' => $jawaban,
            'mahasiswa_id' => $id,
         ));
    }
    
    public function actionForget()
    {
        unset(Yii::app()->session['progress']);
        unset(Yii::app()->session['mahasiswa_validasi']);
            unset(Yii::app()->session['mahasiswa_nim']);
            unset(Yii::app()->session['mahasiswa_nama']);
            unset(Yii::app()->session['mahasiswa_tgl_lhr']);
            unset(Yii::app()->session['mahasiswa_thn_lulus']);
            unset(Yii::app()->session['temp_answer_a']);
        $model = new Mahasiswa;
        $form = $form = new CForm('application.views.site.forget',$model);
        $this->performAjaxValidation($model,'form-id');
        if(isset($_POST['Mahasiswa']))
        {
           $nim = $_POST['Mahasiswa']['nama'];
           /**
             * Web service untan
             * masih disable web servicenya
             */
            $response = file_get_contents('http://service.untan.ac.id:7777/datasnap/rest/tservermethods1/carialumni/'.$nim);
            //$response = '{"result":[[{"nim":"B11100009","nama":"LILY","prodi":"Manajemen"},{"nim":"B11195138","nama":"LILY WIDYANTI","prodi":"Manajemen"},{"nim":"B31101460","nama":"TAN KIAT HWANG\/LILY SUNARYO","prodi":"Manajemen"},{"nim":"B51101432","nama":"LILY SUSANNA","prodi":"Akuntansi"},{"nim":"H12102047","nama":"Lily Pebrianti","prodi":"Fisika"},{"nim":"D11102115","nama":"Lily Muslihatun","prodi":"Sipil"},{"nim":"D11195115","nama":"LILY ELYANTI","prodi":"Sipil"},{"nim":"D11196049","nama":"LILY AGUSLIANAWATY","prodi":"Sipil"},{"nim":"E01108014","nama":"LILYANA SIHOMBING","prodi":"Ilmu Adminitrasi Negara"},{"nim":"D03105041","nama":"LILY AMALIA","prodi":"Informatika"},{"nim":"F11109005","nama":"LILY","prodi":"Pendidikan Bhs dan Sas Ind dan Daerah"},{"nim":"F11199134","nama":"LILY PUJARYANTI","prodi":"Pendidikan Bhs dan Sas Ind dan Daerah"}]]}';   
            $d = CJSON::decode($response);
            $this->render('forget_list',array('model' => $d,'nim' => $nim));

        }
        else
        {
            $this->render('//layouts/form_forget',array('form' => $form,'model' => $model));
        }
    }
    
    /**
     * Controller ini hanya berguna untuk memasukkan data yang diklik oleh user di halaman forget ke dalam session
     * untuk dibawa ke site/login
     */
    public function actionForgetClick($nim,$nama,$prodi)
    {
        if($nim == NULL || $nama == NULL || $prodi == NULL)
        {
            $this->redirect(array('forget'));
        }
         Yii::app()->session['mahasiswa_nim'] = $nim;
         Yii::app()->session['mahasiswa_nama'] = $nama;
         Yii::app()->session['mahasiswa_prodi'] = $prodi;
         $this->redirect(array('login'));
    }
    
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
         {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
            }
     }
     
     public function SaveAnswer()
     {
         $mhs_id = NULL;
         $mhs = Yii::app()->session['data_mahasiswa'];
         $model = new Mahasiswa;
         foreach($mhs as $key => $val):
             $model->$key = $val;
         endforeach;
         if($model->save())
         {
             $mhs_id = $model->id;
         }   
         foreach ($this->array as $row) :
             $session = Yii::app()->session[$row];
             if($session != NULL)
             {
                 foreach($session as $answer):
                     $model = new Jawaban;
                     $model->mahasiswa = $mhs_id;
                     foreach($answer as $key => $val) :
                        $model->$key = $val;
                     endforeach;
                     echo $model->save();
                 endforeach;
             }
         endforeach;
         if($mhs_id != NULL)
         {
             return $mhs_id;
         }
         else
         {
             return 0;
         }
         
     }
}
