 <div class="col-md-4 col-md-offset-4"> 
     <div class="login-panel panel panel-default">
         <div class="panel-heading">
             <h3 class="panel-title">Lupa NIM</h3>
         </div>
         <div class="panel-body">
             <?php $this->widget('bootstrap.widgets.BsAlert');?>
             <?php echo $form->renderBegin(array('class' => 'form'));?>
                 <fieldset>
                     <?php foreach($form->getElements() as $element) :?>
                     <div class="form-group">
                         <?php echo $element->renderInput();?>
                     </div>
                     <?php endforeach;?>
                     <!-- Change this to a button or input when using this as a form -->
                     <div align="center">
                         <?php echo $form->renderButtons();?>
                     </div>
                 </fieldset>
                 <?php echo $form->renderEnd();?>
         </div>
     </div>
 </div>