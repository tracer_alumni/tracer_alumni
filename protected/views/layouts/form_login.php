<div class="row well">
     <div class="col-md-8">
                <br>
                <img src="../images/logos.png" class="img-responsive" alt="Logo Untan" width="150">
                <h1>Selamat Datang</h1>
                <h4>Selamat datang di Halaman input kuisioner, Tracer Study alumni Universitas Tanjungpura Pontianak. <br><br>
                <strong>Petunjuk Pengisian:</strong><br>
                <ul>
                    <li>Isikan jawaban sesuai dengan kondisi yang ada</li>
                    <li>Pastikan anda menjawab semua pertanyaan hingga selesai</li>
                    <li>Bila anda telah mengerti petujuk pengisian, silahkan isikan identitas anda sebagai alumni pada form login</li>
                </ul>
                </h4>
     </div>

     <div class="col-md-4"> 
         <div class="login-panel panel panel-default">
             <div class="panel-heading">

                 <h3 class="panel-title">
                     <?php echo isset(Yii::app()->user->id) ?  'Masukkan Identitas Mahasiswa' : 'Silahkan Login';?>
                     <span class="pull-right" style="margin-top: 3px"><?php echo CHtml::link('Lupa NIM ?',array('forget'),array('class' => 'forget'));?></span>
                 </h3>
             </div>
             <div class="panel-body">
                 <?php $this->widget('bootstrap.widgets.BsAlert');?>
                 <?php echo $form->renderBegin(array('class' => 'form'));?>
                     <fieldset>
                         <?php foreach($form->getElements() as $element) :?>
                         <div class="form-group">
                             <?php echo $element->renderInput();?>
                         </div>
                         <?php endforeach;?>
                         <!-- Change this to a button or input when using this as a form -->
                         <div align="center">
                             <?php echo $form->renderButtons();?>
                         </div>
                     </fieldset>
                     <?php echo $form->renderEnd();?>
             </div>
         </div>
     </div>
 </div>

