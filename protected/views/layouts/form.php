<?php echo $form->renderBegin(array('class' => 'form','enableAjaxValidation' => true));?>
<div class="col-lg-6">
<?php $this->widget('bootstrap.widgets.BsAlert');?>
<table class="detail-view table table-condensed col-lg-6 " border="0">
    <?php foreach($form->getElements() as $element) :?>
    <tr>
        <td style="border: none"><label><?php echo $element->getLabel();?></label></td>
        <td style="border: none">:</td>
        <td style="border: none">
            <?php echo $element->renderInput();?>
            <?php echo $element->renderError();?>
        </td>
    </tr>
    <?php endforeach;?>
</table>
</div>
<div class="col-lg-12" align="center">
    <table class="detail-view table table-condensed col-lg-12 " border="0">
        <td><div align="center"><?php echo $form->renderButtons();?></div></td>
    </table>
</div>
<?php echo $form->renderEnd();?>