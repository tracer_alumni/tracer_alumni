<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <?php Yii::app()->bootstrap->register();?>
        <?php Yii::app()->clientScript->registerCoreScript("jquery");?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/sb-admin-2.js',  CClientScript::POS_END);?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/metisMenu.min.js',  CClientScript::POS_END);?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="creator" content="IT Konsultant"/>
        <meta name="language" content="id" />
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl.'/images/favico.png';?>"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/metisMenu.min.css';?>"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/font-awesome/css/font-awesome.min.css';?>" type="text/css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/style.css';?>"/>
       
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
         <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl.'/css/sb-admin-2.css';?>"/>
        
    </head>
    
    <body>
        <div id="front-end-wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header ">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php echo CHtml::link(Yii::app()->name, array('login'),array('class' => 'navbar-brand'));?>
                </div>
                
                <ul class="nav navbar-top-links navbar-right">
                    <?php if(!Yii::app()->user->isGuest) : ?>
                    <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Pengguna">
                                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><?php echo CHtml::link('<i class="fa fa-user fa-fw"></i> '.Yii::app()->user->name,array('/Administrator/dashboard/index'));?></li>
                                <li><?php echo CHtml::link('<i class="fa fa-shield fa-fw"></i> Ganti katasandi',array('/Administrator/admin/changePassword'));?></li>
                                <li class="divider"></li>
                                <li><?php echo CHtml::link('<i class="fa fa-sign-out fa-fw"></i> Keluar',array('/Administrator/default/logout'));?></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
            </nav>
            
            <div id="content-wrapper">
                <br/>
                <div class="row">
                    <?php if(isset(Yii::app()->session['progress'])) : $col = Yii::app()->session['progress'];?>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" style="width: <?php echo $col * 100 /6;?>%">
                                <?php echo 'Halaman '.$col.' dari 6';?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $content;?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <hr style="border-bottom: #fff solid 1px"/>
                        <?php echo 'Dibuat Oleh '.CHtml::link('IT Konsultan','http://itkonsultan.co.id/');?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>