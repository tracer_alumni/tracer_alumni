<?php
$this->widget('bootstrap.widgets.BsAlert');
$i=0;
$answer_check = 0;
$answer_radio = 0;
$form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'form-id',
    'enableAjaxValidation'=>true,
    'enableClientValidation' => true)); ?>
<table class="table custom-table">
    <?php foreach($model as $soal) : $i++;?>
    <tr class="form-group">
        <td id="no-border"><?php echo $soal->nomor;?></td>
        <td id="no-border">
            <?php echo $soal->pertanyaan;?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <?php if($soal->tipe == 'D') {
                echo $form->radioButtonList(Jawaban::model(),'pilihan',  CHtml::listData($soal->pilihans,'id','pilihan'),array('name' => 'Jawaban'.$i.'[pilihan]'));
                $answer_radio++;
            } else {
                echo $form->checkboxlist(Jawaban::model(),'pilihan',CHtml::listData($soal->pilihans,'id','pilihan'),array('name' => 'Jawaban'.$i.'[pilihan]'));
                $answer_check++;
            } ?>
        </td>
    </tr>
    <?php if($soal->kolom == 'T') {?>
    <tr>
        <td id="no-border"></td>
        <td id="no-border"><?php echo $form->textField(Jawaban::model(),'keterangan',array('name' => 'Jawaban'.$i.'[keterangan]')); ?></td>
    </tr>
    <?php } else if($soal->kolom == 'I') :?>
    <tr>
        <td id="no-border"></td>
        <td id="no-border">
            <input type="number" name="<?php echo 'Jawaban'.$i.'[keterangan]';?>" class="form-control" placeholder="Keterangan" id="<?php echo 'Jawaban'.$i.'_keterangan';?>" maxlength="200"/>
    </tr>
    <?php endif;?>
    <?php endforeach;?>
    <tr>
        <td colspan="3" align="center">
            <p class="text-left errorMessage" id="message"></p>
            <input type="hidden" name="Jawaban[total]" id="check" value="<?php echo $i;?>"/>
            <input type="hidden" name="Jawaban[validasi_check]" id="checkbox" value="<?php echo $answer_check;?>"/>
            <input type="hidden" name="Jawaban[validasi_radio]" id="radio" value="<?php echo $answer_radio;?>"/>
            <?php echo BsHtml::submitButton('Lanjut',array('color' => BsHtml::BUTTON_COLOR_PRIMARY,'id' => 'submit'));?>
        </td>
    </tr>
</table>
<?php $this->endWidget();?>