<div class="form">
    <?php
    $i=0;
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'form-id',
        'enableAjaxValidation'=>true,
        'enableClientValidation' => false));?>
    <table class="table"align="center">
        <thead>
        <th colspan="6"><?php echo $judul;?>i</th>
        <th></th>
        </thead>
        <?php foreach($model as $soal):
            $i++;?>
        <tr>
            <td><?php echo $i.'.';?></td>
            <td colspan="6">
                <?php echo $soal->pertanyaan;?>
                <input type="hidden" name="<?php echo 'Jawaban'.$i.'[soal]';?>" value="<?php echo $soal->id;?>"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><div class="radio"><label><input value="1" type="radio" name="<?php echo 'Jawaban'.$i.'[jawaban_a]';?>"/>Sangat Kurang</label></div></td>
            <td> <div class="radio"><label><input value="2" type="radio" name="<?php echo 'Jawaban'.$i.'[jawaban_a]';?>"/>Kurang</label></div></td>
            <td><div class="radio"><label><input value="3" type="radio" name="<?php echo 'Jawaban'.$i.'[jawaban_a]';?>"/>Cukup</label></div></td>
            <td><div class="radio"><label><input value="4" type="radio" name="<?php echo 'Jawaban'.$i.'[jawaban_a]';?>"/>Baik</label></div></td>
            <td><div class="radio"><label><input value="5" type="radio" name="<?php echo 'Jawaban'.$i.'[jawaban_a]';?>"/>Sangat Baik</label></div></td>
        </tr>
        <?php endforeach; ?>
        <?php if($this->route != 'site/tracer4') : ?>
        <tr>
            <td colspan="11">
                <p>Saran</p>
                <textarea name="Saran[saran]" placeholder="Saran anda untuk perkembangan UNTAN di masa yang akan datang" rows="3" class="form-control"></textarea>
            </td>
        </tr>
        <?php endif ?>
        <tfoot align="center">
        <th colspan="6">
            <input type="hidden" name="Jawaban[total]" value="<?php echo $i;?>"/>
            <input type="hidden" name="Jawaban[validasi_check]" id="radio" value="<?php echo $i;?>"/>
        <div align="center">
            <?php echo BsHtml::submitButton($keterangan,array('color' => BsHtml::BUTTON_COLOR_PRIMARY,'id' => 'submit'));?>
        </div>
        </th>
        </tfoot>
    </table>
<?php $this->endWidget();?>
</div>