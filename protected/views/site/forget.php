<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nama' => array(
            'type' => 'text',
             'class' => 'form-control',
            'placeholder' => 'Nama',
            'maxlength' => 45,
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton( 'Cari',array(
                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                'name' => 'Click',
            )),
        ),
);
?>