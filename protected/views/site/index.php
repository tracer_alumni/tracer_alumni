<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nim' => array(
            'type' => 'text',
            'disabled' => 1,
            'class' => 'form-control',
            'placeholder' => 'Masukkan NIM',
            'maxlength' => 12,
            'value' => isset(Yii::app()->session['mahasiswa_nim']) ? Yii::app()->session['mahasiswa_nim'] : $this->model->nim,
        ),
        'nama' => array(
            'type' => 'text',
            'disabled' => 1,
            'class' => 'form-control',
            'placeholder' => 'Masukkan nama',
            'maxlength' => 45,
            'value' =>  isset(Yii::app()->session['mahasiswa_nama']) ? Yii::app()->session['mahasiswa_nama'] : $this->model->nama,
        ),
        'prodi' => array(
            'type' => 'dropdownlist',
            'class' => 'form-control',
            'prompt' => Prodi::model()->findByAttributes(array('id' => Yii::app()->session['mahasiswa_prodi']))->nama_prodi,
            'disabled' => 1,
            'items' => CHtml::listData(Prodi::model()->findAll(array('order' => 'nama_prodi ASC')),'id','nama_prodi'),
        ),
        'tanggal_lahir' => array(
            'type' => 'zii.widgets.jui.CJuiDatePicker',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'value' => isset(Yii::app()->session['mahasiswa_tgl_lhr']) ? Yii::app()->session['mahasiswa_tgl_lhr'] : '',
                'disabled' => 1,
            ),
            'htmlOptions' => array(
                'placeHolder' => 'Masukkan tanggal lahir',
                'class' => 'form-control',
            ),
        ),
        'tempat_lahir' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan tempat kelahiran',
            'maxlength' => 45,
            'value' => $this->model->tempat_lahir,
        ),
        'jenis' => array(
            'type' => 'dropdownlist',
            'class' => 'form-control',
            'items' => array('L' => 'Laki-laki','P' => "Perempuan"),
        ),
        'alamat' => array(
            'type' => 'textarea',
            'class' => 'form-control',
            'placeholder' => 'Masukkan alamat',
            'maxlength' => 100,
            'value' => $this->model->alamat,
        ),
        'kota' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan kota',
            'maxlength' => 45,
            'value' => $this->model->kota,
        ),
        'telepon' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan telepon',
            'maxlength' => 15,
            'value' => $this->model->telepon,
        ),
        'email' => array(
            'type' => 'email',
            'class' => 'form-control',
            'placeholder' => 'Masukkan email',
            'maxlength' => 100,
            'value' => $this->model->email,
        ),
        'tahun_masuk' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
            'items' => CHtml::listData(Mahasiswa::model()->getYear('1960'),'id','tahun'),
        ),
        'tahun_lulus' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
            'disabled' => 1,
            'prompt' => Yii::app()->session['mahasiswa_thn_lulus'],
            'items' => CHtml::listData(Mahasiswa::model()->getYear('1960'),'id','tahun'),
        ),
        'instansi' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan instansi tempat bekerja',
            'maxlength' => 100,
            'value' => $this->model->instansi,
        ),
        'telepon_instansi' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan telepon instansi tempat bekerja',
            'maxlength' => 15,
            'value' => $this->model->telepon_instansi,
        ),
        'email_instansi' => array(
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Masukkan email instansi tempat bekerja',
            'maxlength' => 100,
            'value' => $this->model->email_instansi,
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton($this->model->isNewRecord ? 'Lanjut' : 'Simpan',array(
                'name' => 'Click',
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            )),
        ),
);
?>