 <div class=" panel panel-default">
         <div class="panel-heading">
             <h3 class="panel-title">
                 Daftar Pencarian Untuk : <?php echo $nim;?>
                 <span class="pull-right" style="margin-top: 3px"><?php echo CHtml::link('Lupa NIM ?',array('forget'),array('class' => 'forget'));?></span>
             </h3>
         </div>
         <div class="panel-body">
             <?php $this->widget('bootstrap.widgets.BsAlert');?>
             <table class="table table-bordered table-hover">
                 <thead>
                 <td>NIM</td>
                 <td>Nama</td>
                 <td>Prodi</td>
                 <td>Pilih</td>
                 </thead>
             <?php foreach($model["result"][0] as $data) :?>
                 <tr>
                     <td><?php echo $data["nim"];?></td>
                     <td><?php echo $data["nama"];?></td>
                     <td><?php echo $data["prodi"];?></td>
                     <td><a class="btn btn-default btn-primary btn-xs" href="<?php echo Yii::app()->request->baseUrl.'/site/forgetclick/nim/'.$data["nim"].'/nama/'.$data["nama"].'/prodi/'.$data["prodi"];?>">Pilih</a></td>
                 </tr>
             <?php endforeach;?>
             </table>
         </div>
     </div>
