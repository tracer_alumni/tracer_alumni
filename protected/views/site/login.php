<?php
return array(
    'class' => 'form',
    'activeForm' => array(
        'class' => 'CActiveForm',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'id' => 'form-id',
    ),
    'elements' => array(
        'nim' => array(
            'type' => 'text',
             'class' => 'form-control',
            'value' => isset(Yii::app()->session['mahasiswa_nim']) ? Yii::app()->session['mahasiswa_nim'] : '',
            'placeholder' => 'NIM',
            'maxlength' => 12,
        ),
        'nama' => array(
            'type' => 'text',
             'class' => 'form-control',
            'value' =>  isset(Yii::app()->session['mahasiswa_nama']) ? Yii::app()->session['mahasiswa_nama'] : '',
            'placeholder' => 'Nama',
            'maxlength' => 45,
        ),
        'tanggal_lahir' => array(
            'type' => 'zii.widgets.jui.CJuiDatePicker',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeYear'=>true,
                'changeMonth'=>true,
                'yearRange'=>'1969:2011',
            ),
            'htmlOptions' => array(
                'placeHolder' => 'Masukkan tanggal lahir',
                'class' => 'form-control',
            ),
        ),
         'tahun_lulus' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
            'items' => CHtml::listData(Mahasiswa::model()->getYear('1960'),'id','tahun'),
             'prompt' => 'Tahun lulus',
        ),
        'prodi' => array(
            'type' => 'dropdownlist',
             'class' => 'form-control',
            'items' => CHtml::listData(Prodi::model()->findAll(array('order' => 'nama_prodi ASC')),'nama_prodi','nama_prodi'),
            'prompt' => 'Prodi',
            'options' => array(Yii::app()->session['mahasiswa_prodi'] => array('selected' => true),
            ),
        ),
    ),
    'buttons' => array(
            BsHtml::submitButton( 'Masuk',array(
                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                'name' => 'Click',
            )),
        ),
);
?>