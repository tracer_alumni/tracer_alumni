<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Tracer Alumni',
                     'language' => 'id',

	// preloading 'log' component
	'preload'=>array('log'),
                      
                     //path aliases
                     'aliases' => array(
                         'bootstrap' => realpath(__DIR__.'/../extensions/bootstrap'),
                     ),
    
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                                           'bootstrap.*',
                                           'bootstrap.helpers.*',
                                           'bootstrap.behaviors.*',
	),

	'modules'=>array(
                                          'Administrator' => array(
                                            
                                          ),
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'tracer',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
                                                                'class' => 'application.components.EWebUser',
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                                                                'showScriptName' => false,
                                                                'rules' => array(
                                                                        'index' => 'site/index',
                                                                        'tracer' => 'site/tracer',
                                                                        'tracer2' => 'site/tracer2',
                                                                        'tracer3' => 'site/tracer3',
                                                                        'tracer4' => 'site/tracer4',
                                                                        'tracer5' => 'site/tracer5',
                                                                ),
                    /*
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
                     * 
                     */
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
                 *      
                 */
		// uncomment the following to use a MySQL database
		

                'db'=>array(
                        'connectionString' => 'mysql:host=localhost;dbname=tracer_alumni',
                        'emulatePrepare' => true,
                        'username' => 'root',
                        'password' => 'ape kate kaulah ju ju ###',
                        'charset' => 'utf8',
                ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                                          'bootstrap' => array(
                                              'class' => 'bootstrap.components.BsApi',
                                          ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
